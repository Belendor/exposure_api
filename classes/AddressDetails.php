<?php
/**
 * Created by PhpStorm.
 * User: barteld.logghe
 * Date: 6-12-2019
 * Time: 10:34
 */
namespace Classes;

use JsonSerializable;

class AddressDetails implements JsonSerializable
{
	/**
	 * @var string $street
	 */
	private $street;

	/**
	 * @var int $number
	 */
	private $number;

	/**
	 * @var string $city
	 */
	private $city;

	/**
	 * @var int $postcode
	 */
	private $postcode;

	/**
	 * AddressDetails constructor.
	 *
	 * @param string $street
	 * @param int    $number
	 * @param string $city
	 * @param int    $postcode
	 */
	public function __construct( $street, $number, $city, $postcode )
	{
		$this->street = $street;
		$this->number = $number;
		$this->city = $city;
		$this->postcode = $postcode;
	}

	/**
	 * @return string
	 */
	public function getStreet()
	{
		return $this->street;
	}

	/**
	 * @param string $street
	 */
	public function setStreet( $street )
	{
		$this->street = $street;
	}

	/**
	 * @return int
	 */
	public function getNumber()
	{
		return $this->number;
	}

	/**
	 * @param int $number
	 */
	public function setNumber( $number )
	{
		$this->number = $number;
	}

	/**
	 * @return string
	 */
	public function getCity()
	{
		return $this->city;
	}

	/**
	 * @param string $city
	 */
	public function setCity( $city )
	{
		$this->city = $city;
	}

	/**
	 * @return int
	 */
	public function getPostcode()
	{
		return $this->postcode;
	}

	/**
	 * @param int $postcode
	 */
	public function setPostcode( $postcode )
	{
		$this->postcode = $postcode;
	}

	/**
	 * Specify data which should be serialized to JSON
	 * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 * which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	public function jsonSerialize()
	{
		return[
			'street'=>$this->street,
			'number'=>$this->number,
			'city'=>$this->city,
			'zipcode'=>$this->postcode
		];
	}
}