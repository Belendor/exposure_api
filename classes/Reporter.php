<?php
/**
 * Created by PhpStorm.
 * User: barteld.logghe
 * Date: 8-10-2019
 * Time: 17:22
 */

namespace Classes;

use JsonSerializable;

class Reporter implements JsonSerializable
{

	/**
	 * @var string $reporter_id
	 */
	private $reporter_id;

	/**
	 * @var string $reporter_email
	 */
	private $reporter_email;

	/**
	 * @var string $reporter_f_name
	 */
	private $reporter_f_name;

	/**
	 * @var string $reporter_l_name
	 */
	private $reporter_l_name;
	/**
	 * @var int $reporter_phone1
	 */
	private $reporter_phone1;
	/**
	 * @var int $reporter_phone2
	 */
	private $reporter_phone2;

	/**
	 * @var AddressDetails $reporter_address
	 */
	private $reporter_address;

	/**
	 * Reporter constructor.
	 *
	 * @param string $reporter_id
	 * @param string $reporter_email
	 * @param string $reporter_f_name
	 * @param string $reporter_l_name
	 * @param string $reporter_phone1
	 * @param string $reporter_phone2
	 */
	public function __construct( $reporter_id, $reporter_email, $reporter_f_name, $reporter_l_name,$reporter_phone1,$reporter_phone2 )
	{
		$this->reporter_id = $reporter_id;
		$this->reporter_email = $reporter_email;
		$this->reporter_f_name = $reporter_f_name;
		$this->reporter_l_name = $reporter_l_name;
		$this->reporter_phone1 = $reporter_phone1;
		$this->reporter_phone2 = $reporter_phone2;
		$this->reporter_address=(new AddressDetails("kattenstraat",80,"Roeselare",8800));
	}

	/**
	 * @return string
	 */
	public function getReporterId()
	{
		return $this->reporter_id;
	}

	/**
	 * @param string $reporter_id
	 */
	public function setReporterId( $reporter_id )
	{
		$this->reporter_id = $reporter_id;
	}

	/**
	 * @return string
	 */
	public function getReporterEmail()
	{
		return $this->reporter_email;
	}

	/**
	 * @param string $reporter_email
	 */
	public function setReporterEmail( $reporter_email )
	{
		$this->reporter_email = $reporter_email;
	}

	/**
	 * @return string
	 */
	public function getReporterFName()
	{
		return $this->reporter_f_name;
	}

	/**
	 * @param string $reporter_f_name
	 */
	public function setReporterFName( $reporter_f_name )
	{
		$this->reporter_f_name = $reporter_f_name;
	}

	/**
	 * @return string
	 */
	public function getReporterLName()
	{
		return $this->reporter_l_name;
	}

	/**
	 * @param string $reporter_l_name
	 */
	public function setReporterLName( $reporter_l_name )
	{
		$this->reporter_l_name = $reporter_l_name;
	}

	/**
	 * @return int
	 */
	public function getReporterPhone1()
	{
		return $this->reporter_phone1;
	}

	/**
	 * @param int $reporter_phone1
	 */
	public function setReporterPhone1( $reporter_phone1 )
	{
		$this->reporter_phone1 = $reporter_phone1;
	}

	/**
	 * @return int
	 */
	public function getReporterPhone2()
	{
		return $this->reporter_phone2;
	}

	/**
	 * @param int $reporter_phone2
	 */
	public function setReporterPhone2( $reporter_phone2 )
	{
		$this->reporter_phone2 = $reporter_phone2;
	}

	/**
	 * @return AddressDetails
	 */
	public function getReporterAddress()
	{
		return $this->reporter_address;
	}

	/**
	 * @param AddressDetails $reporter_address
	 */
	public function setReporterAddress( $reporter_address )
	{
		$this->reporter_address = $reporter_address;
	}




	/**
	 * Specify data which should be serialized to JSON
	 * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 * which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	public function jsonSerialize()
	{
		return [
			'reporter_id'     => $this->reporter_id,
			'reporter_email'  => $this->reporter_email,
			'reporter_f_name' => $this->reporter_f_name,
			'reporter_l_name' => $this->reporter_l_name,
			'reporter_phone1' => $this->reporter_phone1,
			'reporter_phone2' => $this->reporter_phone2,
			'address' => $this->getReporterAddress()
		];
	}
}