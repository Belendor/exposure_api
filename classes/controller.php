<?php
/**
 * Created by PhpStorm.
 * User: barteld.logghe
 * Date: 3-7-2019
 * Time: 7:59
 */

namespace Classes;

use Exception;
use PDO;
use PDOException;

require_once __DIR__ . "/../classes/require.php";
require_once __DIR__ . "/../classes/Mailer.php";
require_once __DIR__ . "/../classes/Encrypt.php";
require_once __DIR__ . "/../classes/User.php";
require_once __DIR__ . "/../classes/Label.php";
require_once __DIR__ . "/../classes/Reporter.php";
require_once __DIR__ . "/../classes/Problem.php";
require_once __DIR__ . "/../classes/ReturnMessage.php";
require_once __DIR__ . "/../classes/Service.php";
require_once __DIR__ . "/../classes/AddressDetails.php";

class controller
{

	/**@var  Encrypt $encrypt */
	public $encrypt;

	/** @var Mailer $mailer */
	private $mailer;

	/** @var ReturnMessage $returnMessage */
	public $returnMessage;

	/** @var User $user */
	private $user;

	/**@var PDO $localPDO */
	private $localPDO;

	public function __construct()
	{

		$filepath = __DIR__ . "/../../mapsi_ini/config.ini";
		$config = parse_ini_file( $filepath, true );
		$key = $config[ 'encrypt' ][ 'key' ];
		$alg = $config[ 'encrypt' ][ 'alg' ];
		$servername = $config[ 'database' ][ 'servername' ];
		$username = $config[ 'database' ][ 'username' ];
		$password = $config[ 'database' ][ 'password' ];
		$databaseName = $config[ 'database' ][ 'databaseName' ];
		define( "DB_HOST", $servername );
		define( "DB_USER", $username );
		define( "DB_PASS", $password );
		define( "DB_NAME", $databaseName );

		$dsn = 'mysql:host=' . DB_HOST . ';dbname=' . $databaseName;
		$options = [
			PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			PDO::ATTR_EMULATE_PREPARES   => false,
		];
		try {
			$this->localPDO = new PDO( $dsn, $username, $password, $options );
			//start transaction end at "Returnmsg"
			$this->localPDO->beginTransaction();
		} catch ( PDOException $e ) {
			throw new PDOException( $e->getMessage(), (int)$e->getCode() );
		}

		$this->user = new User();
		$this->encrypt = new Encrypt( $key,$alg );
		try {
			$this->mailer = new Mailer();
		} catch ( \PHPMailer\PHPMailer\Exception $e ) {
			//todo implement error handeling.
		}

		//$this->mailer = new Mailer( "", "", "" );
		$this->returnMessage = new ReturnMessage();

	}

	public function startTransaction(){
		try{
			//$this->getLocalPDO()->beginTransaction();
			//$this->getLocalPDO()->beginTransaction();
			$this->getLocalPDO()->rollBack();
			$this->getLocalPDO()->rollBack();
			$this->getLocalPDO()->rollBack();
		}catch (PDOException $e){
			var_dump($e->errorInfo[ 1 ]);

				var_dump($e);
		}
	}


	public function ReturnMsg()
	{
		//end commit all sql
		try{
			$this->getLocalPDO()->commit();
		}catch (PDOException $e){
			var_dump($e->getMessage());
		}
//var_dump($this->getUser());
	//	$this->status = array( "return code" => $return_code, "msg" => $msg );
//var_dump(array("brol" =>$this->getUser()));
		$user_info = json_encode( array("user" =>$this->getUser()), true );
//var_dump($this->getReturnMessage());
		$return_msg = json_encode( $this->getReturnMessage(), true );
		return array_merge( json_decode( $user_info, true ), json_decode( $return_msg, true ) );
	}

	public function getUserInfo()
	{
		try {
			/** @var \PDOStatement $selectedUserStatement */
			$selectedUserStatement = $this->getLocalPDO()->prepare(
				"select * from Mapsi_users where user_id = ?" );
			$selectedUserStatement->bindValue( 1, $this->getUser()->getUserId() );
			if ( $selectedUserStatement->execute() ){
				if ( $selectedUserStatement->rowCount() > 0 ) {
					do {
						$selectedUser = $selectedUserStatement->fetchAll();
					} while ( $selectedUserStatement->nextRowset() && $selectedUserStatement->columnCount() );
					$this->getUser()->setFName( $selectedUser[ 0 ][ 'f_name' ] );
					$this->getUser()->setLName( $selectedUser[ 0 ][ 'l_name' ] );
					$this->getUser()->setUserEmail( $selectedUser[ 0 ][ 'user_email' ] );
					$this->getUser()->setLanguageSetting( $selectedUser[ 0 ][ 'default_language' ] );
					$this->getUser()->setServiceSetting( $selectedUser[ 0 ][ 'service_setting' ] );
					if( $selectedUser[ 0 ][ 'mail_setting' ]==1){
						$this->getUser()->setMailSetting(true);
					}else{
						$this->getUser()->setMailSetting(false);

					}
					//$this->getUser()->setMailSetting( $selectedUser[ 0 ][ 'mail_setting' ] );

					$this->getReturnMessage()->setStatus( 200, "user found" );
				} else {
					$this->getReturnMessage()->setStatus( 202, "no user found" );
				}
			}
		}catch (PDOException $e) {
			//var_dump($e->getMessage());
			$this->getReturnMessage()->setStatus( 202, "Unexpected sql error" );
			$this->getLocalPDO()->rollBack();
		}
	}

	public function getLabelsByUser()
	{
		try {

			if ( $this->CheckIfUserExists() ) {
				$this->getUser()->getUserId();
				/** @var \PDOStatement $selectedUserStatement */
				$selectedUserStatement = $this->getLocalPDO()->prepare(
					"select * from Mapsi_labels where user_id =?" );
				$selectedUserStatement->bindValue( 1, $this->getUser()->getUserId() );

				if ( $selectedUserStatement->execute() ) {
					if ( $selectedUserStatement->rowCount() > 0 ) {

						do {
							$selectedUser = $selectedUserStatement->fetchAll();
						} while ( $selectedUserStatement->nextRowset() && $selectedUserStatement->columnCount() );
						$this->getReturnMessage()->setStatus( 200, "Labels found" );
						foreach ( $selectedUser as $label ) {
							$this->getUser()->addLabels( new Label( $label[ 'label_id' ], $label[ 'label_name' ], $label[ 'user_id' ] ) );
						}
					} else {
						$this->getReturnMessage()->setStatus( 202, "no labels found for user_id" );
					}
				}
			}
		} catch ( PDOException $e ) {
			var_dump( $e->getMessage() );
			$this->getReturnMessage()->setStatus( 202, "Unexpected sql error" );
			$this->getLocalPDO()->rollBack();

		}

	}


	public function getServicesByUserId()
	{
		try {
			if ( $this->CheckIfUserExists() ) {
				$this->getUser()->getUserId();
				/** @var \PDOStatement $selectedUserStatement */
				$selectedUserStatement = $this->getLocalPDO()->prepare(
					"select * from Mapsi_services where user_id =?" );
				$selectedUserStatement->bindValue( 1, $this->getUser()->getUserId() );

				if ( $selectedUserStatement->execute() ) {
					if ( $selectedUserStatement->rowCount() > 0 ) {
						do {
							$selectedUser = $selectedUserStatement->fetchAll();
						} while ( $selectedUserStatement->nextRowset() && $selectedUserStatement->columnCount() );
						$this->getReturnMessage()->setStatus( 200, "services found" );
						foreach ( $selectedUser as $service ) {
							$this->getUser()->addServices( new Service( $service[ 'service_id' ], $service[ 'user_id' ], $service[ 'service_description' ], $service[ 'service_email' ] ) );
						}
					} else {
						$this->getReturnMessage()->setStatus( 202, "no services found for user_id" );
					}
				}
			}
		} catch ( PDOException $e ) {
			//var_dump($e->getMessage());
			$this->getReturnMessage()->setStatus( 202, "Unexpected sql error" );
			$this->getLocalPDO()->rollBack();
		}
	}

	public function getReportersByUser()
	{
		try {

			if ( $this->CheckIfUserExists() ) {

				/** @var \PDOStatement $selectedUserStatement */
				$selectedUserStatement = $this->getLocalPDO()->prepare(
					"select * from Mapsi_reporters where user_id =?" );
				$selectedUserStatement->bindValue( 1, $this->getUser()->getUserId() );

				if ( $selectedUserStatement->execute() ) {
					if ( $selectedUserStatement->rowCount() > 0 ) {
						do {
							$selectedUser = $selectedUserStatement->fetchAll();
						} while ( $selectedUserStatement->nextRowset() && $selectedUserStatement->columnCount() );
						$this->getReturnMessage()->setStatus( 200, "Reporters found" );
						foreach ( $selectedUser as $reporter ) {
							$this->getUser()->addReporters( new Reporter( $reporter[ 'reporter_id' ], $reporter[ 'reporter_email' ], $reporter[ 'reporter_f_name' ], $reporter[ 'reporter_l_name' ],$reporter[ 'reporter_phone1' ],$reporter[ 'reporter_phone2' ] ) );
						}
					} else {
						$this->getReturnMessage()->setStatus( 202, "no reporters found for user_id" );
					}
				}
			}
		} catch ( PDOException $e ) {
			//var_dump($e->getMessage());
			$this->getReturnMessage()->setStatus( 202, "Unexpected sql error" );
			$this->getLocalPDO()->rollBack();
		}
	}

	public function getProblemsByUser()
	{
		try {
			if ( $this->CheckIfUserExists() ) {
				/** @var \PDOStatement $selectedUserStatement */
				$selectedUserStatement = $this->getLocalPDO()->prepare(
					"select * from Mapsi_problems where user_id =?" );
				$selectedUserStatement->bindValue( 1, $this->getUser()->getUserId() );

				if ( $selectedUserStatement->execute() ) {
					if ( $selectedUserStatement->rowCount() > 0 ) {
						do {
							$selectedUser = $selectedUserStatement->fetchAll();
						} while ( $selectedUserStatement->nextRowset() && $selectedUserStatement->columnCount() );
						$this->getReturnMessage()->setStatus( 200, "Problems found" );

						foreach ( $selectedUser as $problem ) {
							//todo forsee a fix if no lastnotified
							$reporterIds = $this->FillReporterIds( $problem[ 'problem_id' ] );
							$labelIds = $this->FillLabelIds( $problem[ 'problem_id' ] );
							$serviceIds = $this->FillServiceIds( $problem[ 'problem_id' ] );
							//todo fetch + fill in ids


							$this->getUser()->addProblems( new Problem( $problem[ 'problem_id' ], $problem[ 'user_id' ], $serviceIds, $reporterIds, $labelIds, $problem[ 'problem_description' ], $problem[ 'problem_status' ], new \DateTime( $problem[ 'creation_date' ] ), new \DateTime( $problem[ 'last_notified' ] ) ) );
						}
					} else {
						$this->getReturnMessage()->setStatus( 202, "no problems found for user_id" );
					}
				}
			}
		} catch ( PDOException $e ) {
			//var_dump($e->getMessage());
			$this->getReturnMessage()->setStatus( 202, "Unexpected sql error" );
			$this->getLocalPDO()->rollBack();
		} catch ( Exception $e ) {
			//var_dump($e->getMessage());
			$this->getReturnMessage()->setStatus( 202, "Unexpected dateTime error" );
		}
	}

	public function getProblemListByUser(){
		try {
			if ( $this->CheckIfUserExists() ) {
				/** @var \PDOStatement $selectedUserStatement */
				$selectedUserStatement = $this->getLocalPDO()->prepare(
					"select * from Mapsi_problems where user_id =?" );
				$selectedUserStatement->bindValue( 1, $this->getUser()->getUserId() );

				if ( $selectedUserStatement->execute() ) {
					if ( $selectedUserStatement->rowCount() > 0 ) {
						do {
							$selectedUser = $selectedUserStatement->fetchAll();
						} while ( $selectedUserStatement->nextRowset() && $selectedUserStatement->columnCount() );
						$this->getReturnMessage()->setStatus( 200, "Problems found" );

						foreach ( $selectedUser as $problem ) {



							$this->getUser()->addProblems( new Problem( $problem[ 'problem_id' ], $problem[ 'user_id' ], "", "", "", $problem[ 'problem_description' ], $problem[ 'problem_status' ], new \DateTime( $problem[ 'creation_date' ] ), new \DateTime( $problem[ 'last_notified' ] ) ) );
						}
					} else {
						$this->getReturnMessage()->setStatus( 202, "no problems found for user_id" );
					}
				}
			}
		} catch ( PDOException $e ) {
			//var_dump($e->getMessage());
			$this->getReturnMessage()->setStatus( 205, $e->getMessage() );
			$this->getLocalPDO()->rollBack();
		} catch ( Exception $e ) {
			//var_dump($e->getMessage());
			$this->getReturnMessage()->setStatus( 202, "Unexpected dateTime error" );
		}	}


	/**
	 * @param $problem_id
	 */
	public function getOneProblemByUser( $problem_id )
	{
		try {

			if ( $this->CheckIfUserExists() ) {
				/** @var \PDOStatement $selectedUserStatement */
				$selectedUserStatement = $this->getLocalPDO()->prepare(
					"select * from Mapsi_problems where user_id =? AND problem_id =?" );
				$selectedUserStatement->bindValue( 1, $this->getUser()->getUserId() );
				$selectedUserStatement->bindValue( 2, $problem_id );

				if ( $selectedUserStatement->execute() ) {
					if ( $selectedUserStatement->rowCount() > 0 ) {
						do {
							$selectedUser = $selectedUserStatement->fetchAll();
						} while ( $selectedUserStatement->nextRowset() && $selectedUserStatement->columnCount() );
						$this->getReturnMessage()->setStatus( 200, "Problem found" );

						foreach ( $selectedUser as $problem ) {
							//todo forsee a fix if no lastnotified
							$reporterIds = $this->FillReporterIds( $problem[ 'problem_id' ] );
							$labelIds = $this->FillLabelIds( $problem[ 'problem_id' ] );
							$serviceIds = $this->FillServiceIds( $problem[ 'problem_id' ] );
							//todo fetch + fill in ids
							$this->getUser()->addProblems( new Problem( $problem[ 'problem_id' ], $problem[ 'user_id' ], $serviceIds, $reporterIds, $labelIds, $problem[ 'problem_description' ], $problem[ 'problem_status' ], new \DateTime( $problem[ 'creation_date' ] ), new \DateTime( $problem[ 'last_notified' ] ) ) );
							//$this->FillProblemIds($problem['problem_id']);
						}
					} else {
						$this->getReturnMessage()->setStatus( 202, "Problem not found" );
					}
				}
			}
		} catch ( PDOException $e ) {
			//var_dump($e->getMessage());
			$this->getReturnMessage()->setStatus( 202, "Unexpected sql error" );
			$this->getLocalPDO()->rollBack();
		} catch ( Exception $e ) {
			//var_dump($e->getMessage());
			$this->getReturnMessage()->setStatus( 202, "Unexpected dateTime error" );
		}
	}

	/**
	 * @param $user_pass
	 */
	public function Login( $user_pass )
	{
		try{
			/** @var \PDOStatement $loginStatement */
			$loginStatement = $this->getLocalPDO()->prepare( "select * from Mapsi_users where user_email=?" );
			$loginStatement->bindValue( 1, $this->getUser()->getUserEmail() );

			if ( $loginStatement->execute() ){
				if ( $loginStatement->rowCount() > 0 ) {
					do {
						$createdUser = $loginStatement->fetchAll();
					} while ( $loginStatement->nextRowset() && $loginStatement->columnCount() );
					$hash = $createdUser[ 0 ]['user_pass'];

					if (password_verify($user_pass, $hash)) {
						$this->getReturnMessage()->setStatus(200,"Succesfull login");
						$this->getUser()->setUserId( $createdUser[ 0 ][ 'user_id' ] );
						$this->getUser()->setLName( $createdUser[ 0 ][ 'l_name' ] );
						$this->getUser()->setFName( $createdUser[ 0 ][ 'f_name' ] );
					} else {
						$this->getReturnMessage()->setStatus( 202, "Wrong password" );
					}
				}else{
					$this->getReturnMessage()->setStatus( 202, "Login failed" );
				}
			}
		}catch (PDOException $e){
			//var_dump($e->getMessage());
			$this->getReturnMessage()->setStatus( 202, "Unexpected sql error" );
			$this->getLocalPDO()->rollBack();
		}
	}


	public function getProblemsStatesByUser()
	{
		try {
			if ( $this->CheckIfUserExists() ) {
				/** @var \PDOStatement $selectedUserStatement */
				$selectedUserStatement = $this->getLocalPDO()->prepare(
					"select * from Mapsi_problems where user_id =?" );
				$selectedUserStatement->bindValue( 1, $this->getUser()->getUserId() );

				if ( $selectedUserStatement->execute() ) {
					if ( $selectedUserStatement->rowCount() > 0 ) {
						do {
							$selectedUser = $selectedUserStatement->fetchAll();
						} while ( $selectedUserStatement->nextRowset() && $selectedUserStatement->columnCount() );
						$this->getReturnMessage()->setStatus( 200, "Problems found" );

						foreach ( $selectedUser as $problem ) {

							$this->getUser()->addProblems( new Problem( $problem[ 'problem_id' ], $problem[ 'user_id' ], "", "", "", $problem[ 'problem_description' ], $problem[ 'problem_status' ], new \DateTime( $problem[ 'creation_date' ] ), new \DateTime( $problem[ 'last_notified' ] ) ) );
						}
					} else {
						$this->getReturnMessage()->setStatus( 202, "no problems found for user_id" );
					}
				}
			}
		} catch ( PDOException $e ) {
			//var_dump($e->getMessage());
			$this->getReturnMessage()->setStatus( 202, "Unexpected sql error" );
			$this->getLocalPDO()->rollBack();
		} catch ( Exception $e ) {
			//var_dump($e->getMessage());
			$this->getReturnMessage()->setStatus( 202, "Unexpected dateTime error" );
		}
	}

	public function getLabelInfo( $label_id )
	{
		try {
			if ( $this->CheckIfUserExists() ) {
				$this->GetLabel( $label_id );
				$this->getReturnMessage()->setStatus(200,"Label found");
			}
		} catch ( PDOException $e ) {
			//var_dump($e->getMessage());
			$this->getLocalPDO()->rollBack();
			$this->getReturnMessage()->setStatus( 205, $e->getMessage() );
		} catch ( Exception $e ) {
			$this->getReturnMessage()->setStatus( 202, $e->getMessage() );
			//var_dump($e->getMessage());
		}
	}

	public function getServiceInfo( $service_id )
	{
		try {
			if ( $this->CheckIfUserExists() ) {
				$this->GetService( $service_id );
				$this->getReturnMessage()->setStatus(200,"Service found");
			}
		} catch ( PDOException $e ) {
			//var_dump($e->getMessage());
			$this->getReturnMessage()->setStatus( 205, $e->getMessage() );
			$this->getLocalPDO()->rollBack();
		} catch ( Exception $e ) {
			$this->getReturnMessage()->setStatus( 202, $e->getMessage() );
			//var_dump($e->getMessage());
		}
	}

	public function getReporterInfo( $reporter_id )
	{
		try {
			if ( $this->CheckIfUserExists() ) {
				$this->GetReporter( $reporter_id );
				$this->getReturnMessage()->setStatus(200,"Contact found");
			}
		} catch ( PDOException $e ) {
			//var_dump($e->getMessage());
			$this->getReturnMessage()->setStatus( 205, $e->getMessage() );
			$this->getLocalPDO()->rollBack();
		} catch ( Exception $e ) {
			$this->getReturnMessage()->setStatus( 202, $e->getMessage() );
			//var_dump($e->getMessage());
		}

	}



	public function StatusCheck()
	{
		//todo pdo refactor
		/** @var \PDOStatement $statusCheckStatement1 */
		$statusCheckStatement1 = $this->getLocalPDO()->prepare( "SELECT problem_id,problem_description, problem_status, reporter_email, s.service_id, user_email, service_email, user_email, last_notified FROM Mapsi_problems p JOIN Mapsi_users u ON u.user_id = p.user_id JOIN Mapsi_services s ON s.service_id = p.service_id WHERE u.user_id='" . $this->getUser()->getUserId() . "'  AND last_notified <=  SUBDATE(CURDATE(), 1) AND problem_status <> 0 AND last_notified <>'0000-00-00'" );
		/** @var \PDOStatement $statusCheckStatement2 */
		$statusCheckStatement2 = $this->getLocalPDO()->prepare( "SELECT problem_id,problem_description, problem_status, reporter_email, s.service_id, user_email, service_email, user_email, last_notified FROM Mapsi_problems p JOIN Mapsi_users u ON u.user_id = p.user_id JOIN Mapsi_services s ON s.service_id = p.service_id WHERE u.user_id='" . $this->getUser()->getUserId() . "'  AND problem_status =1 AND creation_date <= SUBDATE(CURDATE(), 1) and (last_notified IS NULL OR last_notified ='0000-00-00')" );
		/** @var \PDOStatement $updateNotifyCheckStatement */
		//$updateNotifyCheckStatement =  $this->getLocalPDO()->prepare( "UPDATE Exposure_problem SET last_notified = CURRENT_DATE() where problem_id in ($query)" );
		//todo

	}
	//end query functions

	//Command functions
	/**
	 * @param $password
	 *
	 * @return false|string|array
	 */
	public function CreateUser( $password )
	{
		$user = $this->getUser();
		if ( isset( $user ) ) {
			try {
				/** @var \PDOStatement $createUserStatement */
				$createUserStatement = $this->getLocalPDO()->prepare( "CALL create_user(?,?,?,?)" );
				$createUserStatement->bindValue( 1, $this->getUser()->getFName() );
				$createUserStatement->bindValue( 2, $this->getUser()->getLName() );
				$createUserStatement->bindValue( 3, $this->getUser()->getUserEmail() );
				$password_hash = password_hash( $password, PASSWORD_BCRYPT );
				var_dump($password_hash);
				$createUserStatement->bindValue( 4, $password_hash );

				//to see the created hash
				//var_dump($password_hash);
				if ( $createUserStatement->execute() ) {
					if ( $createUserStatement->rowCount() > 0 ) {
						do {
							$createdUser = $createUserStatement->fetchAll();
						} while ( $createUserStatement->nextRowset() && $createUserStatement->columnCount() );
						$this->getReturnMessage()->setStatus( 200, "User created" );

						$this->getUser()->setUserId( $createdUser[ 0 ][ 'user_id' ] );
					} else {
						$this->getReturnMessage()->setStatus( 202, "creation user failed" );
					}
				}
			} catch ( PDOException $e ) {
				//var_dump($e->getMessage());
				if ( $e->errorInfo[ 1 ] == 1062 ) {
					$this->getReturnMessage()->setStatus( 202, "Duplicate entry" );
				} else {
					$this->getReturnMessage()->setStatus( 202, $e->getMessage() );
				}
				$this->getLocalPDO()->rollBack();
			}
		}
		return "";
	}

	public function ModifyUser()
	{
		try {
			if ( $this->CheckIfUserExists()) {
				$service=$this->getUser()->getServiceSetting();

				if(isset($service) && $this->CheckIfUserHasService($this->getUser()->getServiceSetting()) ||is_null($service)){
					/** @var \PDOStatement $updateUserStatement */
					$updateUserStatement = $this->getLocalPDO()->prepare( "CALL modify_user(?,?,?,?,?,?,?)" );
					$updateUserStatement->bindValue( 1, $this->getUser()->getUserId() );
					$updateUserStatement->bindValue( 2, $this->getUser()->getFName() );
					$updateUserStatement->bindValue( 3, $this->getUser()->getLName() );
					$updateUserStatement->bindValue( 4, $this->getUser()->getUserEmail() );
					$updateUserStatement->bindValue(5,$this->getUser()->getMailSetting());
					$updateUserStatement->bindValue(6,$this->getUser()->getServiceSetting());
					$updateUserStatement->bindValue(7,$this->getUser()->getLanguageSetting());
					if ( $updateUserStatement->execute() ) {
						if ( $updateUserStatement->rowCount() > 0 ) {
							do {
								$modifiedUser = $updateUserStatement->fetchAll();
							} while ( $updateUserStatement->nextRowset() && $updateUserStatement->columnCount() );
							$this->getReturnMessage()->setStatus( 200, "User modified" );

							$this->FillUserData( $modifiedUser[ 0 ][ 'user_id' ], $modifiedUser[ 0 ][ 'user_email' ], $modifiedUser[ 0 ][ 'l_name' ], $modifiedUser[ 0 ][ 'f_name' ] );
						} else {
							$this->getReturnMessage()->setStatus( 202, "modify user failed" );
						}
					}
				}else{
					$this->getReturnMessage()->setStatus(202, "selected service not part of current user");
				}
			}else{
				$this->getReturnMessage()->setStatus(202, "user not found");
			}
		} catch ( PDOException $e ) {
			var_dump($e->getMessage());
			if ( $e->errorInfo[ 1 ] == 1062 ) {
				$this->getReturnMessage()->setStatus( 202, "Duplicate entry" );
			} else {
				var_dump($e);
				$this->getReturnMessage()->setStatus( 202, $e->getMessage());
			}
			$this->getLocalPDO()->rollBack();
		}
	}

	/**
	 * @param string $label_description
	 */
	public function CreateLabel( $label_description )
	{
		try {
			if ( $this->CheckIfUserExists() ) {
				/** @var \PDOStatement $createLabelStatement */
				$createLabelStatement = $this->getLocalPDO()->prepare( "CALL create_label(?,?)" );
				$createLabelStatement->bindValue( 1, $this->getUser()->getUserId() );
				$createLabelStatement->bindValue( 2, $label_description );
				if ( $createLabelStatement->execute() ) {
					if ( $createLabelStatement->rowCount() > 0 ) {
						do {
							$createdLabel = $createLabelStatement->fetchAll();
						} while ( $createLabelStatement->nextRowset() && $createLabelStatement->columnCount() );
						$this->getUser()->CreateLabel( $createdLabel[ 0 ][ 'label_id' ], $createdLabel[ 0 ][ 'label_name' ] );
						$this->getReturnMessage()->setStatus( 200, "Label created" );
					} else {
						$this->getReturnMessage()->setStatus( 202, "Create label failed" );
					}
				}
			}

		} catch ( PDOException $e ) {
			//var_dump($e->getMessage());
			if ( $e->errorInfo[ 1 ] == 1062 ) {
				$this->getReturnMessage()->setStatus( 202, "Duplicate entry" );
			} else {
				$this->getReturnMessage()->setStatus( 202, $e->getMessage() );
			}
			$this->getLocalPDO()->rollBack();
		}
	}

	/**
	 * @param string $label_id
	 * @param string $label_description
	 */
	public function ModifyLabel( $label_id, $label_description )
	{
		try {
			if ( $this->CheckIfUserExists() && $this->CheckIfLabelExists( $label_id ) ) {
				/** @var \PDOStatement $modifyLabelStatement */
				$modifyLabelStatement = $this->getLocalPDO()->prepare( "CALL modify_label(?,?,?)" );
				$modifyLabelStatement->bindValue( 1, $label_id );
				$modifyLabelStatement->bindValue( 2, $this->getUser()->getUserId() );
				$modifyLabelStatement->bindValue( 3, $label_description );

				if ( $modifyLabelStatement->execute() ) {
					if ( $modifyLabelStatement->rowCount() > 0 ) {
						do {
							$modifiedLabel = $modifyLabelStatement->fetchAll();
						} while ( $modifyLabelStatement->nextRowset() && $modifyLabelStatement->columnCount() );
						$this->getUser()->CreateLabel( $modifiedLabel[ 0 ][ 'label_id' ], $modifiedLabel[ 0 ][ 'label_name' ] );
						$this->getReturnMessage()->setStatus( 200, "Label modified" );
					} else {
						$this->getReturnMessage()->setStatus( 202, "Modify label failed" );
					}
				}
			}
		} catch ( PDOException $e ) {
			//var_dump($e->getMessage());
			if ( $e->errorInfo[ 1 ] == 1062 ) {
				$this->getReturnMessage()->setStatus( 202, "Duplicate entry" );
			} else {
				$this->getReturnMessage()->setStatus( 202, "Unexpected sql error" );
			}
			$this->getLocalPDO()->rollBack();
		}
	}

	/**
	 * @param string $service_email
	 * @param string $service_description
	 */
	public function CreateService( $service_email, $service_description )
	{
		try {
			if ( $this->CheckIfUserExists() ) {
				/** @var \PDOStatement $createServiceStatement */
				$createServiceStatement = $this->getLocalPDO()->prepare( "CALL create_service(?,?,?)" );
				$createServiceStatement->bindValue( 1, $this->getUser()->getUserId() );
				$createServiceStatement->bindValue( 2, $service_description );
				$createServiceStatement->bindValue( 3, $service_email );

				if ( $createServiceStatement->execute() ) {
					if ( $createServiceStatement->rowCount() > 0 ) {
						do {
							$createdService = $createServiceStatement->fetchAll();
						} while ( $createServiceStatement->nextRowset() && $createServiceStatement->columnCount() );
						$this->getUser()->CreateService( $createdService[ 0 ][ 'service_id' ], $createdService[ 0 ][ 'service_description' ], $createdService[ 0 ][ 'service_email' ] );
						$this->getReturnMessage()->setStatus( 200, "Service created" );
					} else {
						$this->getReturnMessage()->setStatus( 202, "Create service failed" );
					}
				}
			}
		} catch ( PDOException $e ) {
			//var_dump($e->getMessage());
			if ( $e->errorInfo[ 1 ] == 1062 ) {
				$this->getReturnMessage()->setStatus( 202, "Duplicate entry" );
			} else {
				$this->getReturnMessage()->setStatus( 202, "Unexpected sql error" );
			}
			$this->getLocalPDO()->rollBack();
		}
	}

	/**
	 * @param string $service_id
	 * @param string $service_email
	 * @param string $service_description
	 */
	public function ModifyService( $service_id, $service_email, $service_description )
	{
		try {
			if ( $this->CheckIfUserExists() && $this->CheckIfServiceExists( $service_id ) ) {

				/** @var \PDOStatement $modifyServiceStatement */
				$modifyServiceStatement = $this->getLocalPDO()->prepare( "CALL modify_service(?,?,?,?)" );
				$modifyServiceStatement->bindValue( 1, $service_id );
				$modifyServiceStatement->bindValue( 2, $this->getUser()->getUserId() );
				$modifyServiceStatement->bindValue( 3, $service_description );
				$modifyServiceStatement->bindValue( 4, $service_email );

				if ( $modifyServiceStatement->execute() ) {
					if ( $modifyServiceStatement->rowCount() > 0 ) {
						do {
							$modifiedService = $modifyServiceStatement->fetchAll();
						} while ( $modifyServiceStatement->nextRowset() && $modifyServiceStatement->columnCount() );
						$this->getUser()->CreateService( $modifiedService[ 0 ][ 'service_id' ], $modifiedService[ 0 ][ 'service_description' ], $modifiedService[ 0 ][ 'service_email' ] );
						$this->getReturnMessage()->setStatus( 200, "Service modified" );
					} else {
						$this->getReturnMessage()->setStatus( 202, "Service modified failed" );
					}
				}
			}
		} catch ( PDOException $e ) {
			//var_dump($e->getMessage());
			if ( $e->errorInfo[ 1 ] == 1062 ) {
				$this->getReturnMessage()->setStatus( 202, "Duplicate entry" );
			} else {
				$this->getReturnMessage()->setStatus( 202, "Unexpected sql error" );
			}
			$this->getLocalPDO()->rollBack();
		}
	}

	/**
	 * @param string $reporter_f_name
	 * @param string $reporter_l_name
	 * @param string $reporter_email
	 * @param int $reporter_phone1
	 * @param int $reporter_phone2
	 */
	public function CreateReporter( $reporter_f_name, $reporter_l_name, $reporter_email,$reporter_phone1,$reporter_phone2 )
	{
		try {
			$this->CreateReporterPrivate($reporter_f_name, $reporter_l_name, $reporter_email,$reporter_phone1,$reporter_phone2);
		} catch ( PDOException $e ) {
			var_dump($e->getMessage());
			if ( $e->errorInfo[ 1 ] == 1062 ) {
				$this->getReturnMessage()->setStatus( 202, "Duplicate entry" );
			} else {
				$this->getReturnMessage()->setStatus( 202, $e->getMessage() );
			}
			$this->getLocalPDO()->rollBack();
		}
	}

	/**
	 * @param string $reporter_id
	 * @param string $reporter_f_name
	 * @param string $reporter_l_name
	 * @param string $reporter_email
	 * @param string $reporter_phone1
	 * @param string $reporter_phone2
	 */
	public function ModifyReporter( $reporter_id, $reporter_f_name, $reporter_l_name, $reporter_email,$reporter_phone1, $reporter_phone2 )
	{
		try {
			if ( $this->CheckIfUserExists() && $this->CheckIfReporterExists( $reporter_id ) ) {
				/** @var \PDOStatement $modifiedReporterStatement */
				$modifiedReporterStatement = $this->getLocalPDO()->prepare( "CALL modify_reporter(?,?,?,?,?,?,?)" );

				$modifiedReporterStatement->bindValue( 1, $reporter_id );
				$modifiedReporterStatement->bindValue( 2, $this->getUser()->getUserId() );
				$modifiedReporterStatement->bindValue( 3, $reporter_f_name );
				$modifiedReporterStatement->bindValue( 4, $reporter_l_name );
				$modifiedReporterStatement->bindValue( 5, $reporter_email );
				$modifiedReporterStatement->bindValue( 6, $reporter_phone1 );
				$modifiedReporterStatement->bindValue( 7, $reporter_phone2 );


				if ( $modifiedReporterStatement->execute() ) {
					if ( $modifiedReporterStatement->rowCount() > 0 ) {
						do {
							$modifiedReporter = $modifiedReporterStatement->fetchAll();
						} while ( $modifiedReporterStatement->nextRowset() && $modifiedReporterStatement->columnCount() );
						$this->getUser()->CreateReporter( $modifiedReporter[ 0 ][ 'reporter_id' ], $modifiedReporter[ 0 ][ 'reporter_f_name' ], $modifiedReporter[ 0 ][ 'reporter_l_name' ], $modifiedReporter[ 0 ][ 'reporter_email' ], $modifiedReporter[ 0 ][ 'phone1' ],$modifiedReporter[ 0 ][ 'phone2' ] );
						$this->getReturnMessage()->setStatus( 200, "Reporter modified" );
					} else {
						$this->getReturnMessage()->setStatus( 202, "Modify reporter failed" );
					}
				}
			}
		} catch ( PDOException $e ) {
			//var_dump($e->getMessage());
			if ( $e->errorInfo[ 1 ] == 1062 ) {
				$this->getReturnMessage()->setStatus( 202, "Duplicate entry" );
			} else {
				$this->getReturnMessage()->setStatus( 202, $e->getMessage() );
			}
			$this->getLocalPDO()->rollBack();
		}
	}

	/**
	 * @param $password
	 */
	public function ChangePass( $password )
	{
		try {
			if ( $this->CheckIfUserExists() ) {
				/** @var \PDOStatement $updateUserStatement */
				$updateUserStatement = $this->getLocalPDO()->prepare( "CALL modify_user_pass(?,?)" );
				$updateUserStatement->bindValue( 1, $this->getUser()->getUserId() );
				$password_hash = password_hash( $password, PASSWORD_BCRYPT );
				$updateUserStatement->bindValue( 2, $password_hash );
				//to see the created hash
				//var_dump($password_hash);

				if ( $updateUserStatement->execute() ) {
					if ( $updateUserStatement->rowCount() > 0 ) {
						do {
							$modifiedUser = $updateUserStatement->fetchAll();
						} while ( $updateUserStatement->nextRowset() && $updateUserStatement->columnCount() );
						$this->getReturnMessage()->setStatus( 200, "Password changed" );
						$this->FillUserData( $modifiedUser[ 0 ][ 'user_id' ], $modifiedUser[ 0 ][ 'user_email' ], $modifiedUser[ 0 ][ 'l_name' ], $modifiedUser[ 0 ][ 'f_name' ] );
					}
				}
			}
		} catch ( PDOException $e ) {
			//var_dump($e->getMessage());
			if ( $e->errorInfo[ 1 ] == 1062 ) {
				$this->getReturnMessage()->setStatus( 202, "Duplicate entry" );
			} else {
				$this->getReturnMessage()->setStatus( 202, "Unexpected sql error" );
			}
			$this->getLocalPDO()->rollBack();
		}
	}

	/**
	 * @param string[] $service_ids
	 * @param string[] $reporter_ids
	 * @param string[] $label_ids
	 * @param string   $problem_description
	 * @param int      $problem_status
	 * @param array[]  $reporters
	 */
	public function CreateProblem( $service_ids, $reporter_ids, $label_ids, $problem_description,$problem_status, $reporters )
	{
		try {
			if ( $this->CheckIfUserExists() ) {
				$this->getUserInfo();

				//services
				foreach ( $service_ids as $serviceId ) {
					$this->GetService( $serviceId );
				}
				//labels
				foreach ( $label_ids as $labelId ) {
					$this->GetLabel( $labelId );
				}
				foreach($reporter_ids as $reporterId){
					$this->CheckIfReporterExists($reporterId);
				}

				//reporters
				/*foreach ($reporter_ids as $reporterId){
					$this->GetReporter($reporterId);
				}*/

				//create new reporter when creating a problem
				//else add reporter to user
				//todo there needs to be a check between $reporters & reporter_id
				//todo something is missing.
				foreach ( $reporters as $reporter ) {
					if ( !isset( $reporter[ 'reporter_id' ] ) ) {
						$reporter_id = $this->CreateReporterPrivate( $reporter[ 'reporter_f_name' ], $reporter[ 'reporter_l_name' ], $reporter[ 'reporter_email' ],$reporter[ 'reporter_phone1' ],$reporter[ 'reporter_phone2' ]  );
						if ( $reporter_id !== "" ) {
							$reporter_ids[] = $reporter_id;
						}
					} else {
						$this->GetReporter( $reporter[ 'reporter_id' ] );
					}
				}
				//todo if 1 of these fail, rollback but also remove all stuff from user object.
				//$this->getLocalPDO()->commit();
				//create problem
				/** @var \PDOStatement $createProblemStatement */
				//todo implement change problem_status
				$createProblemStatement = $this->getLocalPDO()->prepare( "CALL create_problem('" . $this->getUser()->getUserId() . "','" . $problem_description ."')" );
				if ($createProblemStatement->execute() ) {
					if($createProblemStatement->rowCount() > 0){
						do {
							$createdProblem = $createProblemStatement->fetchAll();

						} while ( $createProblemStatement->nextRowset() && $createProblemStatement->columnCount() );
						$problem_id = $createdProblem[ 0 ][ 'problem_id' ];

						//reset relations
						//create new relations
						$this->ResetRelations( $problem_id, $service_ids, $label_ids, $reporter_ids );
						$this->getUser()->CreateProblem( $createdProblem[ 0 ][ 'problem_id' ], $service_ids, $reporter_ids, $label_ids, $createdProblem[ 0 ][ 'problem_description' ], $createdProblem[ 0 ][ 'problem_status' ], new \DateTime( $createdProblem[ 0 ][ 'creation_date' ] ), new \DateTime( $createdProblem[ 0 ][ 'last_notified' ] ) );
						$this->getReturnMessage()->setStatus(200,"problem created");
					}
				}
			}
			$this->sendCreationMails();

		} catch ( PDOException $e ) {
			//var_dump($e);
			if($this->getReturnMessage()->getStatus()['return code'] ==200 ||$this->getReturnMessage()->getStatus()['return code']==null ){
				$this->getReturnMessage()->setStatus(202, $e->getMessage());
				//todo clear user object
			}
			try {
				$this->getLocalPDO()->rollBack();
			}catch (PDOException $e){
				$this->getReturnMessage()->setStatus(203,$e->getMessage() );
				//todo if returnmessage already exists -> dont overwrite
				//todo else new returnmessage
			}
		} catch ( Exception $e ) {
			//var_dump($e->getMessage());
			$this->getReturnMessage()->setStatus( 202, $e->getMessage() );
		}
	}

	/**
	 * @param $problem_id
	 * @param $service_ids
	 * @param $reporter_ids
	 * @param $label_ids
	 * @param $problem_description
	 * @param array[] $reporters
	 */
	public function ModifyProblem( $problem_id, $service_ids, $reporter_ids, $label_ids, $problem_description,$problem_status, $reporters )
	{
		try {
			if ( $this->CheckIfUserExists() && $this->CheckIfProblemExists($problem_id) ) {

				//services
				foreach ( $service_ids as $serviceId ) {
					$this->GetService( $serviceId );
				}
				//labels
				foreach ( $label_ids as $labelId ) {
					$this->GetLabel( $labelId );
				}

				foreach($reporter_ids as $reporterId){
					$this->CheckIfReporterExists($reporterId);
				}
				//reporters
				/*foreach ($reporter_ids as $reporterId){
					$this->GetReporter($reporterId);
				}*/

				//create new reporter when creating a problem
				//else add reporter to user
				//todo there needs to be a check between $reporters & reporter_id
				//todo something is missing.
				foreach ( $reporters as $reporter ) {
					if ( !isset( $reporter[ 'reporter_id' ] ) ) {
						$reporter_id = $this->CreateReporterPrivate( $reporter[ 'reporter_f_name' ], $reporter[ 'reporter_l_name' ], $reporter[ 'reporter_email' ],$reporter['reporter_phone1'],$reporter['reporter_phone2'] );
						if ( $reporter_id !== "" ) {
							$reporter_ids[] = $reporter_id;
						}
					} else {

						$this->GetReporter( $reporter[ 'reporter_id' ] );
					}
				}
			}
			//todo if 1 of these fail, rollback but also remove all stuff from user object.
			//$this->getLocalPDO()->commit();
			//create problem

			//todo implement change problem_status
			/** @var \PDOStatement $createProblemStatement */
			$createProblemStatement = $this->getLocalPDO()->prepare( "CALL modify_problem('" . $problem_id . "','" . $this->getUser()->getUserId() . "','" . $problem_description . "')" );

			if ( $createProblemStatement->execute() ) {
				if ( $createProblemStatement->rowCount() > 0 ) {
					do {
						$createdProblem = $createProblemStatement->fetchAll();
					} while ( $createProblemStatement->nextRowset() && $createProblemStatement->columnCount() );
					$problem_id = $createdProblem[ 0 ][ 'problem_id' ];
					//reset relations
					//create new relations
					$this->ResetRelations( $problem_id, $service_ids, $label_ids, $reporter_ids );
					$this->getUser()->CreateProblem( $createdProblem[ 0 ][ 'problem_id' ], $service_ids, $reporter_ids, $label_ids, $createdProblem[ 0 ][ 'problem_description' ], "", new \DateTime( $createdProblem[ 0 ][ 'creation_date' ] ), new \DateTime( $createdProblem[ 0 ][ 'last_notified' ] ) );
					$this->getReturnMessage()->setStatus(200,"problem modified");
				}
			}
		} catch ( PDOException $e ) {
			//var_dump($e);
			if ( $this->getReturnMessage()->getStatus()[ 'return code' ] == 200 || $this->getReturnMessage()->getStatus()[ 'return code' ] == NULL ) {
				$this->getReturnMessage()->setStatus( 202, $e->getMessage() );
				//todo clear user object
			}
			try {
				$this->getLocalPDO()->rollBack();
			} catch ( PDOException $e ) {
				$this->getReturnMessage()->setStatus( 203, $e->getMessage() );
				//todo if returnmessage already exists -> dont overwrite
				//todo else new returnmessage
			}
		} catch ( Exception $e ) {
			//var_dump($e->getMessage());
			$this->getReturnMessage()->setStatus( 202, $e->getMessage() );
		}
	}

	//Command functions end



	//private functions
	/**
	 * @param $problem_id
	 * @param $service_ids
	 * @param $label_ids
	 * @param $reporter_ids
	 *
	 */
	private function ResetRelations( $problem_id, $service_ids, $label_ids, $reporter_ids )
	{

		/** @var \PDOStatement $deleteProblemStatement */
		$deleteProblemStatement = $this->getLocalPDO()->prepare(
			"delete from Mapsi_problem_reporter where problem_id =?" );
		$deleteProblemStatement->bindValue(1,$problem_id);
		$deleteProblemStatement->execute();

		/** @var \PDOStatement $deleteProblemStatement */
		$deleteProblemStatement = $this->getLocalPDO()->prepare(
			"delete from Mapsi_problem_label where problem_id =?" );
		$deleteProblemStatement->bindValue(1,$problem_id);
		$deleteProblemStatement->execute();

		/** @var \PDOStatement $deleteProblemStatement */
		$deleteProblemStatement = $this->getLocalPDO()->prepare(
			"delete from Mapsi_problem_service where problem_id =?" );
		$deleteProblemStatement->bindValue(1,$problem_id);
		$deleteProblemStatement->execute();

		foreach ( $service_ids as $service_id ) {
			/** @var \PDOStatement $createServiceProblemStatement */
			$createServiceProblemStatement = $this->getLocalPDO()->prepare(
				"insert into Mapsi_problem_service(problem_id, service_id)values(?,?)" );
			$createServiceProblemStatement->bindValue(1,$problem_id);
			$createServiceProblemStatement->bindValue(2,$service_id);
			$createServiceProblemStatement->execute();
		}

		foreach ( $label_ids as $label_id ) {
			/** @var \PDOStatement $createLabelProblemStatement */
			$createLabelProblemStatement = $this->getLocalPDO()->prepare(
				"insert into Mapsi_problem_label(problem_id, label_id)values(?,?)" );
			$createLabelProblemStatement->bindValue(1,$problem_id);
			$createLabelProblemStatement->bindValue(2,$label_id);
			$createLabelProblemStatement->execute();
		}

		foreach ( $reporter_ids as $reporter_id ) {
			/** @var \PDOStatement $createReporterProblemStatement */
			$createReporterProblemStatement = $this->getLocalPDO()->prepare(
				"insert into Mapsi_problem_reporter(problem_id, reporter_id)values(?,?)" );
			$createReporterProblemStatement->bindValue(1,$problem_id);
			$createReporterProblemStatement->bindValue(2,$reporter_id);
			$createReporterProblemStatement->execute();
		}
	}

	/**
	 * @param $service_id
	 */
	private function GetService( $service_id){
		/** @var \PDOStatement $getServiceStatement */
		$getServiceStatement = $this->getLocalPDO()->prepare( "select * from Mapsi_services where service_id=? AND user_id=?" );
		$getServiceStatement->bindValue( 1, $service_id );
		$getServiceStatement->bindValue(2,$this->getUser()->getUserId());

		if ( $getServiceStatement->execute() ) {
			if ( $getServiceStatement->rowCount() > 0 ) {
				do{
					$service=$getServiceStatement->fetchAll();
				}while($getServiceStatement->nextRowset() && $getServiceStatement->columnCount());
				$s = new Service($service[0]['service_id'],$service[0]['user_id'],$service[0]['service_description'],$service[0]['service_email']);
				$this->getUser()->addServices($s);
			} else {
				throw new PDOException("Service doesn't exist");
			}
		}
	}

	/**
	 * @param $label_id
	 */
	private function GetLabel( $label_id){
		/** @var \PDOStatement $getLabelStatement */
		$getLabelStatement = $this->getLocalPDO()->prepare( "select * from Mapsi_labels where label_id=? AND user_id=?" );
		$getLabelStatement->bindValue( 1, $label_id );
		$getLabelStatement->bindValue(2,$this->getUser()->getUserId());

		if ( $getLabelStatement->execute() ) {
			if ( $getLabelStatement->rowCount() > 0 ) {
				do{
					$label=$getLabelStatement->fetchAll();
				}while($getLabelStatement->nextRowset() && $getLabelStatement->columnCount());
				$l = new Label($label[0]['label_id'],$label[0]['label_name'],$label[0]['user_id']);
				$this->getUser()->addLabels($l);
			} else {
				throw new PDOException("Label doesn't exist");
			}
		}
	}

	/**
	 * @param $reporter_id
	 */
	private function GetReporter( $reporter_id )
	{

		/** @var \PDOStatement $getReporterStatement */
		$getReporterStatement = $this->getLocalPDO()->prepare( "select * from Mapsi_reporters where reporter_id=? AND user_id=?" );
		$getReporterStatement->bindValue( 1, $reporter_id );
		$getReporterStatement->bindValue(2,$this->getUser()->getUserId());

		if ( $getReporterStatement->execute() ) {
			if ( $getReporterStatement->rowCount() > 0 ) {
				do {
					$reporter = $getReporterStatement->fetchAll();
				} while ( $getReporterStatement->nextRowset() && $getReporterStatement->columnCount() );
				$r = new Reporter( $reporter[ 0 ][ 'reporter_id' ], $reporter[ 0 ][ 'reporter_email' ], $reporter[ 0 ][ 'reporter_f_name' ], $reporter[ 0 ][ 'reporter_l_name' ],$reporter[ 0 ][ 'phone1' ],$reporter[ 0 ][ 'phone2' ] );
				$this->getUser()->addReporters( $r );
			} else {
				throw new PDOException( "Reporter doesn't exist" );
			}
		}
	}

	/**
	 * @param $reporter_f_name
	 * @param $reporter_l_name
	 * @param $reporter_email
	 * @param $reporter_phone1
	 * @param $reporter_phone2
	 *
	 * @return string
	 */
	private function CreateReporterPrivate( $reporter_f_name, $reporter_l_name, $reporter_email,$reporter_phone1,$reporter_phone2){
		if ( $this->CheckIfUserExists() ) {
			/** @var \PDOStatement $createReporterStatement */
			$createReporterStatement = $this->getLocalPDO()->prepare( "CALL create_reporter(?,?,?,?,?,?)" );

			$createReporterStatement->bindValue( 1, $this->getUser()->getUserId() );
			$createReporterStatement->bindValue( 2, $reporter_f_name );
			$createReporterStatement->bindValue( 3, $reporter_l_name );
			$createReporterStatement->bindValue( 4, $reporter_email );
			$createReporterStatement->bindValue( 5, $reporter_phone1 );
			$createReporterStatement->bindValue( 6, $reporter_phone2 );
			if ( $createReporterStatement->execute() ) {
				if ( $createReporterStatement->rowCount() > 0 ) {
					do {
						$createdReporter = $createReporterStatement->fetchAll();
					} while ( $createReporterStatement->nextRowset() && $createReporterStatement->columnCount() );

					$this->getUser()->CreateReporter( $createdReporter[ 0 ][ 'reporter_id' ], $createdReporter[ 0 ][ 'reporter_f_name' ], $createdReporter[ 0 ][ 'reporter_l_name' ], $createdReporter[ 0 ][ 'reporter_email' ],$createdReporter[ 0 ][ 'phone1' ],$createdReporter[ 0 ][ 'phone2' ] );
					$this->getReturnMessage()->setStatus( 200, "Reporter created" );

					return $createdReporter[ 0 ][ 'reporter_id' ];
				} else {
					$this->getReturnMessage()->setStatus( 202, "Creation reporter failed" );
				}
			}
		}
	}
	/**
	 * @return bool
	 */
	private function CheckIfUserExists()
	{
		//todo refactor so we throw PDOException?

		/** @var \PDOStatement $checkUserStatement */
			$checkUserStatement = $this->getLocalPDO()->prepare( "select * from Mapsi_users where user_id=?" );
			$checkUserStatement->bindValue( 1, $this->getUser()->getUserId() );
			if ( $checkUserStatement->execute() ) {
				if ( $checkUserStatement->rowCount() > 0 ) {
					return true;
				} else {
					$this->getReturnMessage()->setStatus( 202, "User doesn't exist" );
					return false;
				}
			}
		return false;
	}

	/**
	 * @param $label_id
	 * @return bool
	 */
	//todo refactor so we throw PDOException?
	private function CheckIfLabelExists( $label_id )
	{
		/** @var \PDOStatement $checkLabelStatement */
		$checkLabelStatement = $this->getLocalPDO()->prepare( "select * from Mapsi_labels where label_id=?" );
		$checkLabelStatement->bindValue( 1, $label_id );

		if ( $checkLabelStatement->execute() ) {
			if ( $checkLabelStatement->rowCount() > 0 ) {
				return true;
			} else {
				$this->getReturnMessage()->setStatus( 202, "Label doesn't exist" );
				return false;
			}
		}
		return false;
	}

	/**
	 * @param $service_id
	 * @return bool
	 */
	//todo refactor so we throw PDOException?
	private function CheckIfServiceExists( $service_id )
	{
		/** @var \PDOStatement $checkServiceStatement */
		$checkServiceStatement = $this->getLocalPDO()->prepare( "select * from Mapsi_services where service_id=?" );
		$checkServiceStatement->bindValue( 1, $service_id );

		if ( $checkServiceStatement->execute() ) {
			if ( $checkServiceStatement->rowCount() > 0 ) {
				return true;
			} else {
				$this->getReturnMessage()->setStatus( 202, "Service doesn't exist" );

				return false;
			}
		}
		return false;
	}

	/**
	 * @param $reporter_id
	 * @return bool
	 */

	//todo refactor do we throw PDOException?
	private function CheckIfReporterExists( $reporter_id )
	{
		/** @var \PDOStatement $checkReporterStatement */
		$checkReporterStatement = $this->getLocalPDO()->prepare( "select * from Mapsi_reporters where reporter_id=?" );
		$checkReporterStatement->bindValue( 1, $reporter_id );
		if ( $checkReporterStatement->execute() ) {
			if ( $checkReporterStatement->rowCount() > 0 ) {
				return true;
			} else {
				$this->getReturnMessage()->setStatus( 202, "reporter doesn't exist" );
				throw new PDOException("reporter doesn't exist");
				return false;
			}
		}
		return false;
	}

	/**
	 * @param $problem_id
	 * @return bool
	 */
	private function CheckIfProblemExists($problem_id){
		/** @var \PDOStatement $checkReporterStatement */
		$checkReporterStatement = $this->getLocalPDO()->prepare( "select * from Mapsi_problems where problem_id=?" );
		$checkReporterStatement->bindValue( 1, $problem_id );
		if ( $checkReporterStatement->execute() ) {
			if ( $checkReporterStatement->rowCount() > 0 ) {
				return true;
			} else {
				$this->getReturnMessage()->setStatus( 202, "Problem doesn't exist" );
				return false;
			}
		}
		return false;
	}

	/**
	 * @param $service_id
	 * @return bool
	 */
	private function CheckIfUserHasService($service_id)
	{
		/** @var \PDOStatement $getServiceStatement */
		$getServiceStatement = $this->getLocalPDO()->prepare( "select * from Mapsi_services where service_id=? AND user_id=?" );
		$getServiceStatement->bindValue( 1, $service_id );
		$getServiceStatement->bindValue( 2, $this->getUser()->getUserId() );
		if ( $getServiceStatement->execute() ) {
			if ( $getServiceStatement->rowCount() > 0 ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param $user_id
	 * @param $user_email
	 * @param $l_name
	 * @param $f_name
	 */
	private function FillUserData( $user_id, $user_email, $l_name, $f_name )
	{
		$this->user->setUserId( $user_id );
		$this->user->setUserEmail( $user_email );
		$this->user->setLName( $l_name );
		$this->user->setFName( $f_name );
	}

	/**
	 * @param $problem_id
	 *
	 * @return array
	 * @throws Exception
	 */
	private function FillLabelIds( $problem_id )
	{
		$labels = Array();
		/** @var \PDOStatement $selectLabelsStatement */
		$selectLabelsStatement = $this->getLocalPDO()->prepare(
			"SELECT * FROM Mapsi_problem_label WHERE problem_id=?" );
		$selectLabelsStatement->bindValue( 1, $problem_id );

		if ( $selectLabelsStatement->execute() ) {
			do {
				$selectedUser = $selectLabelsStatement->fetchAll();
			} while ( $selectLabelsStatement->nextRowset() && $selectLabelsStatement->columnCount() );
			foreach ( $selectedUser as $label ) {
				$labels[] = $label[ 'label_id' ];
			}
		}
		return $labels;
	}

	/**
	 * @param $problem_id
	 *
	 * @return array
	 * @throws Exception
	 */
	private function FillServiceIds( $problem_id )
	{
		$services = Array();
		/** @var \PDOStatement $selectServicesStatement */
		$selectServicesStatement = $this->getLocalPDO()->prepare(
			"SELECT * FROM Mapsi_problem_service WHERE problem_id=?" );
		$selectServicesStatement->bindValue(1, $problem_id);
		if ( $selectServicesStatement->execute() ) {
			do {
				$selectedUser = $selectServicesStatement->fetchAll();
			} while ( $selectServicesStatement->nextRowset() && $selectServicesStatement->columnCount() );
			foreach ( $selectedUser as $service ) {
				$services[] = $service[ 'service_id' ];
			}
		}
		return $services;
	}

	/**
	 * @param $problem_id
	 *
	 * @return array
	 * @throws Exception
	 */
	private function FillReporterIds( $problem_id )
	{
		$reporters = Array();
		/** @var \PDOStatement $selectReportersStatement */
		$selectReportersStatement = $this->getLocalPDO()->prepare(
			"SELECT * FROM Mapsi_problem_reporter WHERE problem_id=?" );
		$selectReportersStatement->bindValue(1, $problem_id);

		if ( $selectReportersStatement->execute() ) {
			do {
				$selectedUser = $selectReportersStatement->fetchAll();
			} while ( $selectReportersStatement->nextRowset() && $selectReportersStatement->columnCount() );
			foreach ( $selectedUser as $reporter ) {
				$reporters[] = $reporter[ 'reporter_id' ];
			}
		}
		return $reporters;
	}

	private function sendCreationMails(){
		try {
			//todo has to be called in controller after creation problem.
			$problems= $this->user->getProblems();
			foreach($problems as $problem)
			{
				$problem->getProblemDescription();
				$this->mailer->setSubject($problem->getProblemDescription());
			}
			foreach($this->getUser()->getServices() as $service){

				$this->mailer->addServiceMails($service->getServiceEmail());
			}
			foreach($this->getUser()->getReporters() as $reporter){

				$this->mailer->addReporterMails($reporter->getReporterEmail());
			}

			//$this->mailer->addReporterMails("exposurefromtest@gmail.com");
			//$this->mailer->addReporterMails("receiver@blogghe.be");
			//$this->mailer->addServiceMails("exposureservicetest@gmail.com");
			//$this->mailer->addServiceMails("receiver@blogghe.be");
			$this->mailer->AlertAfterCreationProblem($this->getUser()->getUserEmail());

		} catch ( \PHPMailer\PHPMailer\Exception $e ) {
			var_dump($e->errorMessage());
		}catch (\Exception $e){
			var_dump($e->getMessage());
		}	}

		public function FalseEmail(){
		//$this->setReturnMessage(new ReturnMessage());
		$this->getReturnMessage()->setStatus("500", "no valid email");

		}
	//end private functions


	//getters & setters
	/**
	 * @return Encrypt
	 */
	public function getEncrypt()
	{
		return $this->encrypt;
	}

	/**
	 * @param Encrypt $encrypt
	 */
	public function setEncrypt( $encrypt )
	{
		$this->encrypt = $encrypt;
	}

	/**
	 * @return ReturnMessage
	 */
	public function getReturnMessage()
	{
		return $this->returnMessage;
	}

	/**
	 * @param ReturnMessage $returnMessage
	 */
	public function setReturnMessage( $returnMessage )
	{
		$this->returnMessage = $returnMessage;
	}

	/**
	 *
	 * @return User
	 */
	public function getUser()
	{
		return $this->user;
	}

	/**
	 * @param User $user
	 */
	public function setUser( $user )
	{
		$this->user = $user;
	}

	/**
	 * @return PDO
	 */
	public function getLocalPDO()
	{
		return $this->localPDO;
	}

	/**
	 * @param mixed $localPDO
	 */
	public function setLocalPDO( $localPDO )
	{
		$this->localPDO = $localPDO;
	}

	/**
	 * @return Mailer
	 */
	public function getMailer()
	{
		return $this->mailer;
	}

	/**
	 * @param Mailer $mailer
	 */
	public function setMailer( $mailer )
	{
		$this->mailer = $mailer;
	}


}