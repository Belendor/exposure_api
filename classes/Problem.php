<?php
/**
 * Created by PhpStorm.
 * User: barteld.logghe
 * Date: 8-7-2019
 * Time: 22:14
 */

namespace Classes;

use JsonSerializable;

/**
 * @OA\Schema()
 */
class Problem implements JsonSerializable
{
	/**
	 * Problem id
	 * @OA\Property()
	 * @var string $problem_id
	 */
	private $problem_id;

	/**
	 * User id
	 * @OA\Property()
	 * @var string $user_id
	 */
	//todo remove user_id
	private $user_id;

	/**
	 *  Service id
	 * @OA\Property()
	 * @var array|string
	 */
	private $service_ids = Array();

	/**
	 * Reporter id
	 * @OA\Property()
	 * @var array|string
	 */
	private $reporter_ids = Array();

	/**
	 * Reporter id
	 * @OA\Property()
	 * @var array|string
	 */
	private $label_ids = Array();

	/**
	 * Problem description
	 * @OA\Property()
	 * @var string $problem_description
	 */
	private $problem_description;

	/**
	 * Problem status
	 * @OA\Property()
	 * @var string $problem_status
	 */
	private $problem_status;

	/**
	 * Creation date
	 * @OA\Property()
	 * @var \DateTime $creation_data
	 */
	private $creation_data;

	/**
	 * Last notified
	 * @OA\Property()
	 * @var \DateTime $last_notified
	 */
	private $last_notified;

	/**
	 * Problem constructor.
	 *
	 * @param string       $problem_id
	 * @param string       $user_id
	 * @param array|string $service_ids
	 * @param array|string $reporter_ids
	 * @param array|string $label_ids
	 * @param string       $problem_description
	 * @param string       $problem_status
	 * @param \DateTime    $creation_data
	 * @param \DateTime    $last_notified
	 */
	public function __construct( $problem_id, $user_id, $service_ids, $reporter_ids, $label_ids, $problem_description, $problem_status, \DateTime $creation_data, \DateTime $last_notified )
	{
		//var_dump($reporter_ids);
		//die();
		$this->problem_id = $problem_id;
		$this->user_id = $user_id;
		$this->service_ids = $service_ids;
		$this->reporter_ids = $reporter_ids;
		$this->label_ids=$label_ids;
		$this->problem_description = $problem_description;
		$this->problem_status = $problem_status;
		$this->creation_data = $creation_data;
		$this->last_notified = $last_notified;
		//$this->this->set

	}


	/**
	 * @return string
	 */
	public function getProblemId()
	{
		return $this->problem_id;
	}

	/**
	 * @param string $problem_id
	 */
	public function setProblemId( $problem_id )
	{
		$this->problem_id = $problem_id;
	}

	/**
	 * @return string
	 */
	public function getUserId()
	{
		return $this->user_id;
	}

	/**
	 * @param string $user_id
	 */
	public function setUserId( $user_id )
	{
		$this->user_id = $user_id;
	}

	/**
	 * @return string
	 */
	public function getServiceId()
	{
		return $this->service_ids;
	}

	/**
	 * @param string $service_ids
	 */
	public function setServiceIds( $service_ids )
	{
		$this->service_ids = $service_ids;
	}

	/**
	 * @param array|string $service_ids
	 */
	public function addServiceIds( $service_ids )
	{

	}

	/**
	 * @return string
	 */
	public function getProblemDescription()
	{
		return $this->problem_description;
	}

	/**
	 * @param string $problem_description
	 */
	public function setProblemDescription( $problem_description )
	{
		$this->problem_description = $problem_description;
	}

	/**
	 * @return string
	 */
	public function getProblemStatus()
	{
		return $this->problem_status;
	}

	/**
	 * @param string $problem_status
	 */
	public function setProblemStatus( $problem_status )
	{
		$this->problem_status = $problem_status;
	}

	/**
	 * @return \DateTime
	 */
	public function getCreationData()
	{
		return $this->creation_data;
	}

	/**
	 * @param \DateTime $creation_data
	 */
	public function setCreationData( $creation_data )
	{
		$this->creation_data = $creation_data;
	}

	/**
	 * @return \DateTime
	 */
	public function getLastNotified()
	{
		return $this->last_notified;
	}

	/**
	 * @param \DateTime $last_notified
	 */
	public function setLastNotified( $last_notified )
	{
		$this->last_notified = $last_notified;
	}

	/**
	 * @return array
	 */
	public function getReporterIds()
	{
		return $this->reporter_ids;
	}

	/**
	 * @param array $reporter_ids
	 */
	public function setReporterIds( $reporter_ids )
	{
		$this->reporter_ids = $reporter_ids;
	}

	/**
	 * @param array|string $reporter_ids
	 */
	public function AddReporterIds( $reporter_ids )
	{

	}

	/**
	 * @return array|string
	 */
	public function getLabelIds()
	{
		return $this->label_ids;
	}

	/**
	 * @param array|string $label_ids
	 */
	public function setLabelIds( $label_ids )
	{
		$this->label_ids = $label_ids;
	}

	/**
	 * @param array|string $label_ids
	 */
	public function addLabelIds( $label_ids )
	{
		if ( empty( $this->label_ids ) ) {
			$this->label_ids[ 0 ] = $label_ids;
		} else {
			array_push( $this->label_ids, $label_ids );
		}
	}

	/**
	 * Specify data which should be serialized to JSON
	 * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 * which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	public function jsonSerialize()
	{
		return [
			'problem_id'          => $this->problem_id,
			'user_id'             => $this->user_id,
			'problem_description' => $this->problem_description,
			'problem_status'      => $this->problem_status,
			'creation_data'       => $this->creation_data,
			'last_notified,'      => $this->last_notified,
			'service_ids'         => $this->service_ids,
			'reporter_ids'        => $this->reporter_ids,
			'label_ids'           => $this->label_ids,
		];
	}
}