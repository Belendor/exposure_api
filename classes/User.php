<?php
/**
 * Created by PhpStorm.
 * User: barteld.logghe
 * Date: 8-7-2019
 * Time: 22:14
 */

namespace Classes;

use JsonSerializable;

/**
 * Class User
 * @package Classes
 * @author Barteld Logghe
 */
class User implements JsonSerializable
{

	/**
	 * @var string $user_id
	 */
	private $user_id;

	/**
	 * @var string $f_name
	 */

	private $f_name;
	/**
	 * @var string $l_name
	 */
	private $l_name;

	/**
	 * @var string $user_email
	 */
	private $user_email;

	/**
	 * @var Problem[] $problems
	 */
	private $problems;

	/**
	 * @var Service[] $services
	 */
	private $services;

	/**
	 * @var Reporter[] $reporters
	 */
	private $reporters;

	/**
	 * @var Label[] $labels
	 */
	private $labels;

	/**
	 * @var string $service_setting
	 */
	private $service_setting;

	/**
	 * @var boolean $mail_setting
	 */
	private $mail_setting;

	/**
	 * @var string $language_setting
	 */
	private $language_setting;

	/**
	 * User constructor
	 */
	public function __construct()
	{

	}

	/**
	 * @return Problem[]
	 */
	public function getProblems()
	{
		return $this->problems;
	}

	/**
	 * @param Problem[] $problems
	 */
	public function setProblems( $problems )
	{
		$this->problems = $problems;
	}

	/**
	 * @param Problem[]|Problem $problems
	 */
	public function addProblems( $problems )
	{
		if ( empty( $this->problems ) ) {
			$this->problems[ 0 ] = $problems;
		} else {
			array_push( $this->problems, $problems );
		}
	}

	/**
	 * @return Service[]
	 */
	public function getServices()
	{
		return $this->services;
	}

	/**
	 * @param Service[] $services
	 */
	public function setServices( $services )
	{
		$this->services = $services;
	}

	/**
	 * @param Service|Service[] $services
	 */
	public function addServices( $services )
	{
		if ( empty( $this->services ) ) {
			$this->services[ 0 ] = $services;
		} else {
			array_push( $this->services, $services );
		}
	}

	/**
	 * @return string
	 */
	public function getFName()
	{
		return $this->f_name;
	}

	/**
	 * @param string $f_name
	 */
	public function setFName( $f_name )
	{
		$this->f_name = $f_name;
	}

	/**
	 * @return string
	 */
	public function getLName()
	{
		return $this->l_name;
	}

	/**
	 * @param string $l_name
	 */
	public function setLName( $l_name )
	{
		$this->l_name = $l_name;
	}

	/**
	 * @return string
	 */
	public function getUserEmail()
	{
		return $this->user_email;
	}

	/**
	 * @param string $user_email
	 */
	public function setUserEmail( $user_email )
	{
		$this->user_email = $user_email;
	}

	/**
	 * @return string
	 */
	public function getUserId()
	{
		return $this->user_id;
	}

	/**
	 * @param string $user_id
	 */
	public function setUserId( $user_id )
	{
		$this->user_id = $user_id;
	}

	/**
	 * @return string
	 */
	public function getServiceSetting()
	{
		return $this->service_setting;
	}

	/**
	 * @param string $service_setting
	 */
	public function setServiceSetting( $service_setting )
	{
		$this->service_setting = $service_setting;
	}

	/**
	 * @return boolean
	 */
	public function getMailSetting()
	{
		return $this->mail_setting;
	}

	/**
	 * @param boolean $mail_setting
	 */
	public function setMailSetting( $mail_setting )
	{
		$this->mail_setting = $mail_setting;
	}

	/**
	 * @return string
	 */
	public function getLanguageSetting()
	{
		return $this->language_setting;
	}

	/**
	 * @param string $language_setting
	 */
	public function setLanguageSetting( $language_setting )
	{
		$this->language_setting = $language_setting;
	}


	/**
	 * @return Reporter[]
	 */
	public function getReporters()
	{
		return $this->reporters;
	}

	/**
	 * @param Reporter[] $reporters
	 */
	public function setReporters( $reporters )
	{
		$this->reporters = $reporters;
	}

	/**
	 * @param Reporter|Reporter[] $reporters
	 */
	public function addReporters( $reporters )
	{
		if ( empty( $this->reporters ) ) {
			$this->reporters[ 0 ] = $reporters;
		} else {
			array_push( $this->reporters, $reporters );
		}
	}

	/**
	 * @return Label[]
	 */
	public function getLabels()
	{
		return $this->labels;
	}

	/**
	 * @param Label[] $labels
	 */
	public function setLabels( $labels )
	{
		$this->labels = $labels;
	}

	/**
	 * @param Label|Label[] $labels
	 */
	public function addLabels( $labels )
	{
		if ( empty( $this->labels ) ) {
			$this->labels[ 0 ] = $labels;
		} else {
			array_push( $this->labels, $labels );
		}
	}

	public function CreateService( $service_id, $service_description, $service_email )
	{
		$this->addServices( new Service( $service_id, $this->getUserId(), $service_description, $service_email ) );
	}

	public function CreateReporter( $reporter_id, $reporter_f_name, $reporter_l_name, $reporter_email, $reporter_phone1, $reporter_phone2 )
	{
		$this->addReporters( new Reporter( $reporter_id, $reporter_email, $reporter_f_name, $reporter_l_name, $reporter_phone1, $reporter_phone2 ) );
	}

	public function CreateLabel( $label_id, $label_description )
	{
		$this->addLabels( new Label( $label_id, $label_description, $this->getUserId() ) );
	}

	public function CreateProblem( $problem_id, $service_ids, $reporters_ids, $labels_ids, $problem_description, $problem_status, $creation_date, $last_notified )
	{
		$this->addProblems( new Problem( $problem_id, $this->getUserId(), $service_ids, $reporters_ids, $labels_ids, $problem_description, $problem_status, $creation_date, $last_notified ) );
	}

	/**
	 * Specify data which should be serialized to JSON
	 * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 * which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	public function jsonSerialize()
	{
		return [
			'user_id'    => $this->user_id,
			'f_name'     => $this->f_name,
			'l_name'     => $this->l_name,
			'user_email' => $this->user_email,
			'mail_setting' =>$this->mail_setting,
			'service_setting' => $this->service_setting,
			'language_setting' =>$this->language_setting,
			'problems'   => $this->problems,
			'services'   => $this->services,
			'reporters'  => $this->reporters,
			'labels'     => $this->labels,
		];

	}
}