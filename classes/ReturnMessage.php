<?php
/**
 * Created by PhpStorm.
 * User: barteld.logghe
 * Date: 5-9-2019
 * Time: 14:41
 */

namespace Classes;

//todo contains a template of return message
use JsonSerializable;

class ReturnMessage implements JsonSerializable
{

	private $status;
	private $user;

	/**
	 * @return mixed
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param string  $msg
	 * @param integer $return_code
	 */
	public function setStatus( $return_code, $msg )
	{
		$this->status = array( "return code" => $return_code, "msg" => $msg );
	}

	/**
	 * @param array $array
	 */
	public function setStatusAlternative( $array )
	{
		$this->status = $array;

	}

	/**
	 * @return mixed
	 */
	public function getOtherResponse()
	{
		return $this->other_response;
	}

	/**
	 * @param mixed $other_response
	 */
	public function setOtherResponse( $other_response )
	{
		$this->other_response = $other_response;
	}

	/**
	 * @param controller $controller
	 *
	 * @return array
	 */
	public function generateReturnMsg( $controller )
	{
		$other_response = $controller->getReturnMessage()->getStatus();
		$response = array(
			"return_status" => array(
				"encrypt_status"  => $controller->getEncrypt()->getReturnMessage()->getStatus(),
				"database_status" => $controller->getDatabaseConnection()->getReturnMessage()->getStatus(),
				"mailer_status"   => $controller->getMailer()->getReturnMessage()->getStatus(),
			),
		);

		if ( isset( $other_response ) ) {
			foreach ( $controller->getReturnMessage()->getStatus() as $key => $service ) {
				$response[ $key ] = $service;
			}
		}

		return $response;
	}

	/**
	 * Specify data which should be serialized to JSON
	 * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 * which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	public function jsonSerialize()
	{
		return [
			'return state' => $this->status,
		];
	}
}