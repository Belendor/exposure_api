<?php
/**
 * Created by PhpStorm.
 * User: barteld.logghe
 * Date: 29-11-2019
 * Time: 16:26
 */

namespace Classes;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
require "../vendor/autoload.php";

class Mailer
{
	/**
	 * @var PHPMailer $phpMailer
	 */
	private $phpMailer;

	/**
	 * @var array|string
	 */
	private $reporterMails;

	/**
	 * @var array|string
	 */
	private $serviceMails;
	/**
	 * @var string
	 */
	private $subject;

	/**
	 * Mailer constructor.
	 * @throws \PHPMailer\PHPMailer\Exception
	 */
	public function __construct( )
	{
		$filepath = __DIR__ . "/../../mapsi_ini/config.ini";
		$config = parse_ini_file( $filepath, true );

		$host = $config[ 'mail' ][ 'host' ];
		$username = $config[ 'mail' ][ 'username' ];
		$password = $config[ 'mail' ][ 'password' ];
		$smtpSecure = $config[ 'mail' ][ 'smtpsecure' ];
		$port = $config[ 'mail' ][ 'port' ];
		$from = $config[ 'mail' ][ 'from' ];
		$smtpAuth= $config[ 'mail' ][ 'smptauth' ];

		$this->phpMailer = new PHPMailer();
		//$this->phpMailer->SMTPDebug = SMTP::DEBUG_SERVER;   // Enable verbose debug output
		$this->phpMailer->isSMTP();                         // Send using SMTP
		$this->phpMailer->Host = $host;                     // Set the SMTP server to send through
		$this->phpMailer->SMTPAuth = $smtpAuth;             // Enable SMTP authentication
		$this->phpMailer->Username = $username;             // SMTP username
		$this->phpMailer->Password = $password;             // SMTP password
		$this->phpMailer->SMTPSecure = $smtpSecure;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
		$this->phpMailer->Port = $port;                     // TCP port to connect to
		$this->phpMailer->setFrom( $from );                 // set the sender email
	}

	/**
	 *
	 * @param $userEmail
	 *
	 * @throws \PHPMailer\PHPMailer\Exception
	 * @throws \Exception
	 */
	public function AlertAfterCreationProblem($userEmail){
		//todo exception check if no phpmailer instance
		if(isset($this->phpMailer)){
			foreach ($this->reporterMails as $reporterMail) {
				$this->AlertReporter($reporterMail,$userEmail);
				$this->ResetMail();
			}
			foreach ($this->serviceMails as $serviceMail){
				$this->AlertService($serviceMail,$userEmail);
				$this->ResetMail();
			}
		}else{
			Throw new \Exception("no phpMailer instance");
		}
	}

	/**
	 *
	 */
	public function AlertAfterStatusCheck(){
		//todo exception check if no phpmailer instance

	}

	/**
	 *
	 * @param $reporterMail
	 * @param $userEmail
	 *
	 * @throws \PHPMailer\PHPMailer\Exception
	 * @throws \Exception
	 */
	private function AlertReporter($reporterMail,$userEmail){
		//todo get Array with mails from reporters
		//Recipients
		$this->phpMailer->addAddress($reporterMail);     // Add a recipient
		//$this->phpMailer->addReplyTo('sender@blogghe.be');
		//$this->phpMailer->addCC('exposurefromtest@gmail.com');
		$this->phpMailer->addBCC($userEmail);
		$this->phpMailer->addCustomHeader('X-custom-header: custom-value');

		// Attachments
		//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
		//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

		// Content
		$dt = new \DateTime();
		$this->phpMailer->isHTML(false);                                  // Set email format to HTML
		$this->phpMailer->Subject = 'Reporting a problem to customer ' . $reporterMail . ' about problem "'.  $this->subject.'"';
		//$mail->Body    = 'This is the HTML message body <b>in bold!</b>';
		$this->phpMailer->Body="This is the mail for the customer using " . $this->phpMailer->Host . "send at " . $dt->format('y-m-d H:i:');
		$this->phpMailer->AltBody = 'This is the body in plain text for non-HTML mail customers';

		if(!$this->phpMailer->send()) {
			echo 'Message could not be sent.';
			echo 'Mailer Error: ' . $this->phpMailer->ErrorInfo;
			Throw new \Exception("Reporter mails not send");
		} else {
			//echo 'Message has been sent';
		}

	}

	/**
	 *
	 * @param $serviceMail
	 * @param $userEmail
	 *
	 * @throws \PHPMailer\PHPMailer\Exception
	 * @throws \Exception
	 */
	private function AlertService($serviceMail,$userEmail){
		//todo get Array with mails from Services
		//Recipients
		$this->phpMailer->addAddress($serviceMail);     // Add a recipient
		//$this->phpMailer->addReplyTo('sender@blogghe.be');
		//$this->phpMailer->addCC('receiver@blogghe.be');
		$this->phpMailer->addBCC($userEmail);
		$this->phpMailer->addCustomHeader('X-custom-header: custom-value');

		// Attachments
		//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
		//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

		// Content
		$dt = new \DateTime();
		$this->phpMailer->isHTML(false);                                  // Set email format to HTML
		$this->phpMailer->Subject = 'Reporting a problem to service ' . $serviceMail . ' about problem "'.  $this->subject.'"';
		//$mail->Body    = 'This is the HTML message body <b>in bold!</b>';
		$this->phpMailer->Body="This is the mail for the service using " . $this->phpMailer->Host . "send at " . $dt->format('y-m-d H:i:');
		$this->phpMailer->AltBody = 'This is the body in plain text for non-HTML mail Services';
		if(!$this->phpMailer->send()) {
			echo 'Message could not be sent.';
			echo 'Mailer Error: ' . $this->phpMailer->ErrorInfo;
			Throw new \Exception("Service mails not send");
		} else {
			//echo 'Message has been sent';
		}
	}

	private function ResetMail(){
		$this->phpMailer->clearAllRecipients();
		$this->phpMailer->clearBCCs();
		$this->phpMailer->clearCCs();
		$this->phpMailer->clearAddresses();
		$this->phpMailer->clearAttachments();
		$this->phpMailer->clearCustomHeaders();
		$this->phpMailer->clearReplyTos();
	}
	/**
	 * @return mixed
	 */
	public function getReporterMails()
	{
		return $this->reporterMails;
	}

	/**
	 * @param mixed $reporterMails
	 */
	public function setReporterMails( $reporterMails )
	{
		$this->reporterMails = $reporterMails;
	}

	/**
	 * @return mixed
	 */
	public function getServiceMails()
	{
		return $this->serviceMails;
	}

	/**
	 * @param mixed $serviceMails
	 */
	public function setServiceMails( $serviceMails )
	{
		$this->serviceMails = $serviceMails;
	}

	/**
	 * @return string
	 */
	public function getSubject()
	{
		return $this->subject;
	}

	/**
	 * @param string $subject
	 */
	public function setSubject( $subject )
	{
		$this->subject = $subject;
	}



	/**
	 * @param string $serviceMails
	 */
	public function addServiceMails( $serviceMails){
		if(empty($this->serviceMails)){
			$this->serviceMails[0]= $serviceMails;
		}else{
			array_push($this->serviceMails,$serviceMails);
		}
	}

	/**
	 * @param string $reporterMails
	 */
	public function addReporterMails( $reporterMails){
		if(empty($this->reporterMails)){
			$this->reporterMails[0]= $reporterMails;
		}else{
			array_push($this->reporterMails,$reporterMails);
		}
	}
}