<?php
/**
 * Created by PhpStorm.
 * User: barteld.logghe
 * Date: 8-7-2019
 * Time: 22:14
 */

namespace Classes;

use JsonSerializable;

/**
 * @OA\Schema()
 */
class Service implements JsonSerializable
{
	/**
	 * Service id
	 * @OA\Property()
	 * @var string $service_id
	 */
	private $service_id;

	//todo remove user_id
	/**
	 * User id
	 * @OA\Property()
	 * @var string $user_id
	 */
	private $user_id;

	/**
	 * Service description
	 * @OA\Property()
	 * @var string $service_description
	 */
	private $service_description;

	/**
	 * service email
	 * @OA\Property()
	 * @var string $service_email
	 */
	private $service_email;

	/**
	 * Service constructor.
	 *
	 * @param string $service_id
	 * @param string $user_id
	 * @param string $service_description
	 * @param string $service_email
	 */
	public function __construct( $service_id, $user_id, $service_description, $service_email )
	{
		$this->service_id = $service_id;
		$this->user_id = $user_id;
		$this->service_description = $service_description;
		$this->service_email = $service_email;
	}

	/**
	 * @return string
	 */
	public function getServiceId()
	{
		return $this->service_id;
	}

	/**
	 * @param string $service_id
	 */
	public function setServiceId( $service_id )
	{
		$this->service_id = $service_id;
	}

	/**
	 * @return string
	 */
	public function getUserId()
	{
		return $this->user_id;
	}

	/**
	 * @param string $user_id
	 */
	public function setUserId( $user_id )
	{
		$this->user_id = $user_id;
	}

	/**
	 * @return string
	 */
	public function getServiceDescription()
	{
		return $this->service_description;
	}

	/**
	 * @param string $service_description
	 */
	public function setServiceDescription( $service_description )
	{
		$this->service_description = $service_description;
	}

	/**
	 * @return string
	 */
	public function getServiceEmail()
	{
		return $this->service_email;
	}

	/**
	 * @param string $service_email
	 */
	public function setServiceEmail( $service_email )
	{
		$this->service_email = $service_email;
	}

	/**
	 * Specify data which should be serialized to JSON
	 * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 * which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	public function jsonSerialize()
	{
		return [
			'service_id'          => $this->service_id,
			'user_id'             => $this->user_id,
			'service_description' => $this->service_description,
			'service_email'       => $this->service_email,
		];
	}
}