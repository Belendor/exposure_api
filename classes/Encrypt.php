<?php
/**
 * Created by PhpStorm.
 * User: barteld.logghe
 * Date: 3-7-2019
 * Time: 7:23
 */

namespace Classes;

use Firebase\JWT\JWT;
use Exception;

require_once __DIR__ . "/../classes/ReturnMessage.php";


class Encrypt
{
	/**@var string $key */
	public $key;
	/**@var string $algorithm */
	public $algorithm;

	/**@var  boolean $authorized */
	private $authorized;

	/**@var ReturnMessage $returnMessage */
	private $returnMessage;

	/**
	 * Encrypt constructor.
	 *
	 * @param string $key
	 * @param string  $algorithm
	 */
	public function __construct( $key, $algorithm )
	{

		$this->key = $key;
		$this->algorithm = $algorithm;
		$this->returnMessage = new ReturnMessage();
	}

	/**
	 * @param array $message
	 *
	 * @return string
	 */
	public function Encrypt( $message )
	{
		$jwt = JWT::encode( $message, $this->getKey(), $this->getAlgorithm()[ 0 ] );

		return $jwt;
	}

	/**
	 * @param string $message
	 *
	 * @return array
	 */
	public function Decrypt( $message )
	{
		$decoded = JWT::decode( $message, $this->getKey(), $this->getAlgorithm() );
		$decoded_arr = (array)$decoded;

		return $decoded_arr;
	}

	/**
	 * @return string
	 */
	public function getKey()
	{
		return $this->key;
	}

	/**
	 * @param string $key
	 */
	public function setKey( $key )
	{
		$this->key = $key;
	}

	/**
	 * @return string
	 */
	public function getAlgorithm()
	{
		return $this->algorithm;
	}

	/**
	 * @param string $algorithm
	 */
	public function setAlgorithm( $algorithm )
	{
		$this->algorithm = $algorithm;
	}

	/**
	 * @return mixed
	 */
	public function getAuthorized()
	{
		return $this->authorized;
	}

	/**
	 * @param mixed $authorized
	 */
	public function setAuthorized( $authorized )
	{
		$this->authorized = $authorized;
	}

	/**
	 * @return ReturnMessage
	 */
	public function getReturnMessage()
	{
		return $this->returnMessage;
	}

	/**
	 * @param ReturnMessage $returnMessage
	 */
	public function setReturnMessage( $returnMessage )
	{
		$this->returnMessage = $returnMessage;
	}

	/**
	 * @param $user_id
	 *
	 * @return boolean
	 */
	public function checkAuthorization($user_id)
	{
		$headers = $_SERVER;
		try {

			if ( !key_exists( 'HTTP_AUTH', $headers ) ) {
				$this->getReturnMessage()->setStatus( 1, "No authentication header" );

				return false;
			} else {
				$jwt = $headers[ 'HTTP_AUTH' ];
				$key = "exposure-f56qsd6qs4d856q4sdf4qz4fajoijfdihg)aç!yruça";
				$alg = 'HS256';

				$decoded = JWT::decode( $jwt, $key, array( $alg ) );

				$decoded_arr = (array)$decoded;
				//see value inside token
				//var_dump( $decoded_arr );

				if ( array_key_exists( 'userid', $decoded_arr ) ) {
					//var_dump( $decoded_arr[ 'userid' ] );
					//var_dump( $user_id );
					if ( $decoded_arr[ 'userid' ] == $user_id ) {
						return true;
					} else {
						$this->getReturnMessage()->setStatus( 1, "Wrong authentication header" );

						return false;
					}
				} else {
					$this->getReturnMessage()->setStatus( 1, "Authentication header is wrong" );

					return false;
				}
			}
		} catch ( Exception $e ) {
			$this->getReturnMessage()->setStatus( 1, $e->getMessage() );
		}
	}

	public function GenerateTokenForUser($user_id){
		$new_key = "mapsi-f56qsd6qs4d856q4sdf4qz4fajoijfdihg)aç!yruça";
		$payload = array('userid' => $user_id);
		$jwt = JWT::encode( $payload,$this->key, $this->algorithm );
		var_dump("key for given user_id");
		var_dump( $jwt );
		var_dump($this);
		die();
	}
}