<?php
/**
 * Created by PhpStorm.
 * User: barteld.logghe
 * Date: 8-10-2019
 * Time: 17:37
 */

namespace Classes;

use JsonSerializable;

class Label implements JsonSerializable
{

	private $label_id;

	private $label_name;

	private $user_id;

	/**
	 * Label constructor.
	 *
	 * @param string $label_id
	 * @param string $label_name
	 * @param string $user_id
	 */
	public function __construct( $label_id, $label_name, $user_id )
	{
		$this->label_id = $label_id;
		$this->label_name = $label_name;
		$this->user_id = $user_id;
	}

	/**
	 * @return string
	 */
	public function getLabelId()
	{
		return $this->label_id;
	}

	/**
	 * @param string $label_id
	 */
	public function setLabelId( $label_id )
	{
		$this->label_id = $label_id;
	}

	/**
	 * @return string
	 */
	public function getLabelName()
	{
		return $this->label_name;
	}

	/**
	 * @param string $label_name
	 */
	public function setLabelName( $label_name )
	{
		$this->label_name = $label_name;
	}

	/**
	 * @return string
	 */
	public function getUserId()
	{
		return $this->user_id;
	}

	/**
	 * @param string $user_id
	 */
	public function setUserId( $user_id )
	{
		$this->user_id = $user_id;
	}

	/**
	 * Specify data which should be serialized to JSON
	 * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 * which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	public function jsonSerialize()
	{
		return [
			'label_id'   => $this->label_id,
			'label_name' => $this->label_name,
			'user_id'    => $this->user_id,
		];
	}
}