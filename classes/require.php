<?php

use Firebase\JWT\JWT;

define( "_databaseServername_", "mysqldev" );
define( "_databaseUsername_", "ict_development" );
define( "_databasePassword_", "D3v3l0paSm1l3" );
define( "_databaseName_", "user_bloggh01" );
define( "_port_", "3306" );

define( "_bearer_", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyaWQiOiJiYXJ0ZWxkbG9nZ2hlIiwidGVhbSI6Ik1vZWRlckFuIn0.yw0oPmZJXRPKLybZXme0cC0ZauXSpSEHvvt3IhD9IFA" );
define( "_key_", "exposure-f56qsd6qs4d856q4sdf4qz4fajoijfdihg)aç!yruça" );
define( "_algorithm_", "HS256" );

function checkAuthorization( $headers, $config )
{
	//var_dump($headers);
	//die();

	if ( !key_exists( 'HTTP_AUTH', $headers ) ) {
		return false;
	}
	$jwt = $headers[ 'HTTP_AUTH' ];
	$key = "exposure-f56qsd6qs4d856q4sdf4qz4fajoijfdihg)aç!yruça";
	$alg = 'HS256';

	$decoded = JWT::decode( $jwt, $key, array( $alg ) );
	$decoded_arr = (array)$decoded;

	if ( array_key_exists( 'userid', $decoded_arr ) ) {
		return true;
	}

	return false;
}

function createKey()
{
	$issuedAt = time();
	$payload = array( 'program' => 'exposure', 'iat' => $issuedAt );
	$key = "exposure-f56qsd6qs4d856q4sdf4qz4fajoijfdihg)aç!yruça";
	$alg = 'HS256';
	$jwt = JWT::encode( $payload, $key, $alg );

	return ( $jwt );

}

