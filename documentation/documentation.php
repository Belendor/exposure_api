<?php
/**
 * Created by PhpStorm.
 * User: barteld.logghe
 * Date: 22-11-2019
 * Time: 14:47
 */
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Schema;
/**
 * @OA\Info(title="My First API", version="0.1")
 */




/**
 * @Schema(schema="User",
 *     @Property(
 *     property="user_id",
 *     type="string")),
 * @Property(
 *     property="f_name",
 *     type="string")),
 * @Property(
 *     property="l_name",
 *     type="string")),
 * @Property(
 *     property="user_email",
 *     type="string")),
 * @Property(
 *     property="problems",
 *     type="string")),
 * @Property(
 *     property="services",
 *     type="string")),
 * @Property(
 *     property="reporters",
 *     type="string")),
 * @Property(
 *     property="labels",
 *     type="string"))
 */

/**
 * @Schema(schema="problem",
 * @Property(
 *     property="problem_id",
 *     type="string")),
 * @Property(
 *     property="user_id",
 *     type="string")),
 * @Property(
 *     property="service_ids",
 *     type="string")),
 * @Property(
 *     property="reporters_ids",
 *     type="string")),
 * @Property(
 *     property="labels_id",
 *     type="string")),
 * @Property(
 *     property="problem_description",
 *     type="string")),
 * @Property(
 *     property="problem_status",
 *     type="string")),
 * @Property(
 *     property="creation_date",
 *     type="string")),
 * @Property(
 *     property="user_id",
 *     type="last_notified"))
 */








/**
 * @OA\Get(
 *     path="/get_user_info2",
 *     @OA\Response(response="200", description="An example resource")
 * )
 */
/**
 * @OA\Get(
 *     path="/login2",
 *     @OA\Response(response="200", description="An example resource")
 * )
 */
/**
 * @OA\Get(
 *     path="/get_labels2",
 *     @OA\Response(response="200", description="An example resource")
 * )
 */
/**
 * @OA\Get(
 *     path="/get_services2",
 *     @OA\Response(response="200", description="An example resource")
 * )
 */
/**
 * @OA\Get(
 *     path="/get_reporters2",
 *     @OA\Response(response="200", description="An example resource")
 * )
 */
/**
 * @OA\Get(
 *     path="/get_problems2",
 *     @OA\Response(response="200", description="An example resource")
 * )
 */
/**
 * @OA\Get(
 *     path="/get_all_info",
 *     @OA\Response(response="200", description="An example resource")
 * )
 */
/**
 * @OA\Get(
 *     path="/view_problem",
 *     @OA\Response(response="200", description="An example resource")
 * )
 */


/**
 * @Get(
 *     path="/login2",
 *     @Response(
 *     response="200",
 *     description=""login",
 *     @JsonContent(

 *     @Property(property="User",
 *     ref="#/components/schemas/User),
 *     @Property(property="ReturnState",
 *     ref="#/components/schemas/ReturnState))))
 */


/**
 * @OA\Get(path="/api/posts",
 *   @OA\Response(
 *      response=200,
 *      description="List posts",
 *      @OA\JsonContent(
 *        @OA\Property(
 *          property="posts",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/Post")
 *        ),
 *        @OA\Property(
 *          property="ReturnCode",
 *          ref="#/components/schemas/ReturnCode"
 *        )
 *      )
 *   )
 * )
 */

/**
 * @OA\Schema(
 *   schema="Post",
 *   @OA\Property(
 *     property="id",
 *     type="integer"
 *   )
 * )
 */

/**
 * @OA\Schema(
 *   schema="ReturnCode",
 *   @OA\Property(
 *     property="return code",
 *     type="integer",
 *     minimum=1
 *   ),
 *   @OA\Property(
 *     property="msg",
 *     type="string",
 *     minimum=1
 *   )
 * )
 */

/**
 * @OA\Schema(
 *   schema="MailsSend",
 *   @OA\Property(
 *     property="confirmed_mails",
 *     type="integer",
 *     minimum=1
 *   ),
 *   @OA\Property(
 *     property="ongoing_mails",
 *     type="integer",
 *     minimum=1
 *   )
 * )
 */