<?php
/**
 * Created by PhpStorm.
 * User: barteld.logghe
 * Date: 25-11-2019
 * Time: 8:53
 */

use OpenApi\Annotations\Property;
use OpenApi\Annotations\Schema;
/**
 * @OA\Info(title="My First API", version="0.1")
 */


/**
 * @OA\Get(path="/get_user_info2",
 *     description="Info of user",
 *     @OA\Parameter(
 *     description="user_id parameter",
 *     in="path",
 *     name="user_id",
 *     required=true,
 *     @OA\Schema(
 *     type="string",
 *     format="uuid"
 *     )),
 *     @OA\Response(
 *     response="200",
 *     description="user found",
 *      @OA\JsonContent(
 *        @OA\Property(
 *          property="user",
 *          ref="#/components/schemas/user"
 *        ),
 *        @OA\Property(
 *          property="return state",
 *          ref="#/components/schemas/ReturnCode"
 *        ))))
 */
/**
 * @OA\Get( path="/login2",
 *     description="attempt login",
 *     @OA\Parameter(
 *     description="Email parameter",
 *     in="path",
 *     name="user_email",
 *     required=true,
 *     @OA\Schema(
 *     type="string",
 *     format="uuid"
 *     )),
 *     @OA\Parameter(
 *     description="Password parameter",
 *     in="path",
 *     name="user_pass",
 *     required=true,
 *     @OA\Schema(
 *     type="string"
 *     )),
 *     @OA\Response(response="200", description="Login succesfull",
 *     @OA\JsonContent(
 *        @OA\Property(
 *          property="user",
 *          ref="#/components/schemas/user"
 *        ),
 *        @OA\Property(
 *          property="return state",
 *          ref="#/components/schemas/ReturnCode"
 *        ))))
 */
/**
 * @OA\Get(path="/get_labels2",
 *     description="Get all labels for 1 user",
 *     @OA\Parameter(
 *     description="user_id parameter",
 *     in="path",
 *     name="user_id",
 *     required=true,
 *     @OA\Schema(
 *     type="string",
 *     format="uuid"
 *     )),
 *     @OA\Response(response="200", description="Labels found",
 *     @OA\JsonContent(
 *      @OA\Property(
 *          property="user",
 *          ref="#/components/schemas/user"
 *      ),
 *      @OA\Property(
 *          property="return state",
 *          ref="#/components/schemas/ReturnCode"
 *        ))))
 */
/**
 * @OA\Get(path="/get_services2",
 *     description="Get all services for 1 user",
 *     @OA\Parameter(
 *     description="user_id parameter",
 *     in="path",
 *     name="user_id",
 *     required=true,
 *     @OA\Schema(
 *     type="string",
 *     format="uuid"
 *     )),
 *     @OA\Response(response="200", description="Services found",
 *     @OA\JsonContent(
 *        @OA\Property(
 *          property="user",
 *          ref="#/components/schemas/user"
 *        ),
 *     @OA\Property(
 *          property="return state",
 *          ref="#/components/schemas/ReturnCode"
 *        ))))
 */
/**
 * @OA\Get(path="/get_reporters2",
 *     description="Get all reporters for 1 user",
 *     @OA\Parameter(
 *     description="user_id parameter",
 *     in="path",
 *     name="user_id",
 *     required=true,
 *     @OA\Schema(
 *     type="string",
 *     format="uuid"
 *     )),
 *     @OA\Response(response="200", description="Reporters found",
 *     @OA\JsonContent(
 *     @OA\Property(
 *          property="user",
 *          ref="#/components/schemas/user"
 *        ),
 *        @OA\Property(
 *          property="return state",
 *          ref="#/components/schemas/ReturnCode"
 *        ))))
 */
/**
 * @OA\Get(path="/get_problems2",
 *     description="Get all problems of 1 user",
 *     @OA\Parameter(
 *     description="user_id parameter",
 *     in="path",
 *     name="user_id",
 *     required=true,
 *     @OA\Schema(
 *     type="string",
 *     format="uuid"
 *     )),
 *     @OA\Response(response="200", description="Problems found",
 *      @OA\JsonContent(
 *        @OA\Property(
 *          property="user",
 *          ref="#/components/schemas/user"
 *        ),
 *     @OA\Property(
 *          property="return state",
 *          ref="#/components/schemas/ReturnCode"
 *        ),
 *      )),
 *     @OA\Response(
 *         response="400",
 *         description="Invalid ID supplied"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         description="no problems found"
 *     )
 * )
 */
/**
 * @OA\Get(path="/list_problems",
 *     description="Get all problems of 1 user",
 *     @OA\Parameter(
 *     description="user_id parameter",
 *     in="path",
 *     name="user_id",
 *     required=true,
 *     @OA\Schema(
 *     type="string",
 *     format="uuid"
 *     )),
 *     @OA\Response(response="200", description="Problems found",
 *      @OA\JsonContent(
 *        @OA\Property(
 *          property="user",
 *          ref="#/components/schemas/user"
 *        ),
 *     @OA\Property(
 *          property="return state",
 *          ref="#/components/schemas/ReturnCode"
 *        ),
 *      )),
 *     @OA\Response(
 *         response="400",
 *         description="Invalid ID supplied"
 *     ),
 *     @OA\Response(
 *         response="404",
 *         description="no problems found"
 *     )
 * )
 */

/**
 * @OA\Get(path="/get_all_info",
 *     description="Get all information of 1 user",
 *     @OA\Parameter(
 *     description="user_id parameter",
 *     in="path",
 *     name="user_id",
 *     required=true,
 *     @OA\Schema(
 *     type="string",
 *     format="uuid"
 *     )),
 *     @OA\Response(response="200", description="Labels found",
 *     @OA\JsonContent(
 *        @OA\Property(
 *          property="user",
 *          ref="#/components/schemas/user"
 *        ),
 *        @OA\Property(
 *          property="return state",
 *          ref="#/components/schemas/ReturnCode"
 *        ))))
 */
/**
 * @OA\Get(path="/view_problem",
 *     description="Get information of 1 problem",
 *     @OA\Parameter(
 *     description="user_id parameter",
 *     in="path",
 *     name="user_id",
 *     required=true,
 *     @OA\Schema(
 *     type="string",
 *     format="uuid"
 *     )),
 *     @OA\Response(response="200", description="Problem found",
 *     @OA\JsonContent(
 *        @OA\Property(
 *          property="user",
 *          ref="#/components/schemas/user"
 *        ),
 *        @OA\Property(
 *          property="return state",
 *          ref="#/components/schemas/ReturnCode"
 *        ))))
 */

/**
 * @OA\Post(path="/create_user",
 *     description="",
 *     @OA\RequestBody(
 *         description="Pet object that needs to be added to the store",
 *         required=true,
 *         @OA\JsonContent(ref="#/components/schemas/CreateUser"),
 *     ),
 *     @OA\Response(
 *         response=405,
 *         description="Invalid input",
 *     @OA\JsonContent(
 *        @OA\Property(
 *          property="user",
 *          ref="#/components/schemas/user"),
 *        @OA\Property(
 *          property="return state",
 *          ref="#/components/schemas/ReturnCode"
 *        ))),
 *     security={{"petstore_auth":{"write:pets", "read:pets"}}}
 * )
 */
/**
 * @OA\Schema(schema="CreateUser",
 *     required={"user_pass","user_email"},
 * @Property(
 *     property="user_pass",
 *     type="string"),
 *  @Property(
 *     property="user_email",
 *     type="string",
 *     format="email"))
 */

/**
 * @OA\Post(path="/modify_user",
 *     description="",
 *     @OA\RequestBody(
 *         description="Pet object that needs to be added to the store",
 *         required=true,
 *         @OA\JsonContent(ref="#/components/schemas/ModifyUser"),
 *     ),
 *     @OA\Response(
 *         response=405,
 *         description="Invalid input",
 *     @OA\JsonContent(
 *        @OA\Property(
 *          property="user",
 *          ref="#/components/schemas/user"),
 *     @OA\Property(
 *          property="return state",
 *          ref="#/components/schemas/ReturnCode"
 *        ))),
 *     security={{"petstore_auth":{"write:pets", "read:pets"}}}
 * )
 */
/**
 * @OA\Schema(schema="ModifyUser",
 *     required={"user_id","user_email"},
 * @Property(
 *     property="user_id",
 *     type="string",
 *     format="uuid"),
 * @Property(
 *     property="f_name",
 *     type="string"),
 * @Property(
 *     property="l_name",
 *     type="string"),
 * @Property(
 *     property="user_email",
 *     type="string",
 *     format="email"),
 * @Property(
 *     property="service_setting",
 *     type="string"),
 * @Property(
 *     property="mail_setting",
 *     type="string",
 *     format="email"),
 *  @Property(
 *     property="language_setting",
 *     type="string",
 *     enum={"NL", "FR", "EN"}))
 */

/**
 * @OA\Post(path="/create_label",
 *     description="",
 *     @OA\RequestBody(
 *         description="Pet object that needs to be added to the store",
 *         required=true,
 *         @OA\JsonContent(ref="#/components/schemas/CreateLabel"),
 *     ),
 *     @OA\Response(
 *         response=405,
 *         description="Invalid input",
 *     @OA\JsonContent(
 *        @OA\Property(
 *          property="return state",
 *          ref="#/components/schemas/ReturnCode"
 *        ),
 *        @OA\Property(
 *          property="user",
 *          ref="#/components/schemas/user"))),
 *     security={{"petstore_auth":{"write:pets", "read:pets"}}}
 * )
 */
/**
 * @OA\Schema(schema="CreateLabel",
 *     required={"user_id"},
 * @Property(
 *     property="label_name",
 *     type="string"),
 * @Property(
 *     property="user_id",
 *     type="string",
 *     format="uuid"))
 */

/**
 * @OA\Post(path="/modify_label",
 *     description="",
 *     @OA\RequestBody(
 *         description="Pet object that needs to be added to the store",
 *         required=true,
 *         @OA\JsonContent(ref="#/components/schemas/ModifyLabel"),
 *     ),
 *     @OA\Response(
 *         response=405,
 *         description="Invalid input",
 *     @OA\JsonContent(
 *        @OA\Property(
 *          property="return state",
 *          ref="#/components/schemas/ReturnCode"
 *        ),
 *        @OA\Property(
 *          property="user",
 *          ref="#/components/schemas/user"))),
 *     security={{"petstore_auth":{"write:pets", "read:pets"}}}
 * )
 */
/**
 * @OA\Schema(schema="ModifyLabel",
 *     required={"label_id","user_id"},
 * @Property(
 *     property="label_id",
 *     type="string",
 *     format="uuid"),
 * @Property(
 *     property="label_name",
 *     type="string"),
 * @Property(
 *     property="user_id",
 *     type="string",
 *     format="uuid"))
 */

/**
 * @OA\Post(path="/create_service",
 *     description="",
 *     @OA\RequestBody(
 *         description="Pet object that needs to be added to the store",
 *         required=true,
 *         @OA\JsonContent(ref="#/components/schemas/CreateService"),
 *     ),
 *     @OA\Response(
 *         response=405,
 *         description="Invalid input",
 *     @OA\JsonContent(
 *        @OA\Property(
 *          property="return state",
 *          ref="#/components/schemas/ReturnCode"
 *        ),
 *        @OA\Property(
 *          property="user",
 *          ref="#/components/schemas/user"))),
 *     security={{"petstore_auth":{"write:pets", "read:pets"}}}
 * )
 */
/**
 * @OA\Schema(schema="CreateService",
 *     required={"user_id","service_email"},
 * @Property(
 *     property="user_id",
 *     type="string",
 *     format="uuid"),
 * @Property(
 *     property="service_description",
 *     type="string"),
 * @Property(
 *     property="service_email",
 *     type="string",
 *     format="email"))
 */

/**
 * @OA\Post(path="/modify_service",
 *     description="",
 *     @OA\RequestBody(
 *         description="Pet object that needs to be added to the store",
 *         required=true,
 *         @OA\JsonContent(ref="#/components/schemas/ModifyService"),
 *     ),
 *     @OA\Response(
 *         response=405,
 *         description="Invalid input",
 *     @OA\JsonContent(
 *        @OA\Property(
 *          property="return state",
 *          ref="#/components/schemas/ReturnCode"
 *        ),
 *        @OA\Property(
 *          property="user",
 *          ref="#/components/schemas/user"))),
 *     security={{"petstore_auth":{"write:pets", "read:pets"}}}
 * )
 */
/**
 * @OA\Schema(schema="ModifyService",
 *     required={"service_id","user_id","service_email"},
 * @Property(
 *     property="service_id",
 *     type="string",
 *     format="uuid"),
 * @Property(
 *     property="user_id",
 *     type="string",
 *     format="uuid"),
 * @Property(
 *     property="service_description",
 *     type="string"),
 * @Property(
 *     property="service_email",
 *     type="string",
 *     format="email"))
 */

/**
 * @OA\Post(path="/create_reporter",
 *     description="",
 *     @OA\RequestBody(
 *         description="Pet object that needs to be added to the store",
 *         required=true,
 *         @OA\JsonContent(ref="#/components/schemas/CreateReporter"),
 *     ),
 *     @OA\Response(
 *         response=405,
 *         description="Invalid input",
 *     @OA\JsonContent(
 *        @OA\Property(
 *          property="return state",
 *          ref="#/components/schemas/ReturnCode"
 *        ),
 *        @OA\Property(
 *          property="user",
 *          ref="#/components/schemas/user"))),
 *     security={{"petstore_auth":{"write:pets", "read:pets"}}}
 * )
 */
/**
 * @OA\Schema(schema="CreateReporter",
 *     required={"user_id","reporter_email"},
 * @Property(
 *     property="user_id",
 *     type="string",
 *     format="uuid"),
 * @Property(
 *     property="reporter_f_name",
 *     type="string"),
 * @Property(
 *     property="reporter_l_name",
 *     type="string"),
 * @Property(
 *     property="reporter_email",
 *     type="string",
 *     format="email"),
 * @Property(
 *     property="reporter_phone1",
 *     type="integer"),
 * @Property(
 *     property="reporter_phone2",
 *     type="integer"),
 * @Property(
 *     property="address",
 *     ref="#/components/schemas/Address")
 * )
 */

/**
 * @OA\Post(path="/modify_reporter",
 *     description="",
 *     @OA\RequestBody(
 *         description="Pet object that needs to be added to the store",
 *         required=true,
 *         @OA\JsonContent(ref="#/components/schemas/ModifyReporter"),
 *     ),
 *     @OA\Response(
 *         response=405,
 *         description="Invalid input",
 *     @OA\JsonContent(
 *        @OA\Property(
 *          property="return state",
 *          ref="#/components/schemas/ReturnCode"
 *        ),
 *        @OA\Property(
 *          property="user",
 *          ref="#/components/schemas/user"))),
 *     security={{"petstore_auth":{"write:pets", "read:pets"}}}
 * )
 */
/**
 * @OA\Schema(schema="ModifyReporter",
 * @Property(
 *     property="reporter_id",
 *     type="string",
 *     format="uuid"),
 * @Property(
 *     property="user_id",
 *     type="string",
 *     format="uuid"),
 * @Property(
 *     property="reporter_f_name",
 *     type="string"),
 * @Property(
 *     property="reporter_l_name",
 *     type="string"),
 * @Property(
 *     property="reporter_email",
 *     type="string",
 *     format="email"),
 * @Property(
 *     property="reporter_phone1",
 *     type="integer"),
 * @Property(
 *     property="reporter_phone2",
 *     type="integer"))
 */

/**
 * @OA\Post(path="/change_pass",
 *     description="",
 *     @OA\RequestBody(
 *         description="Pet object that needs to be added to the store",
 *         required=true,
 *         @OA\JsonContent(ref="#/components/schemas/ChangePass"),
 *     ),
 *     @OA\Response(
 *         response=405,
 *         description="Invalid input",
 *     @OA\JsonContent(
 *        @OA\Property(
 *          property="user",
 *          ref="#/components/schemas/user"),
 *     @OA\Property(
 *          property="return state",
 *          ref="#/components/schemas/ReturnCode"
 *        ))),
 *     security={{"petstore_auth":{"write:pets", "read:pets"}}}
 * )
 */
/**
 * @OA\Schema(schema="ChangePass",
 * @Property(
 *     property="user_id",
 *     type="string",
 *     format="uuid"),
 * @Property(
 *     property="user_pass",
 *     type="string"))
 */

/**
 * @OA\Post(path="/create_problem",
 *     description="",
 *     @OA\RequestBody(
 *         description="Pet object that needs to be added to the store",
 *         required=true,
 *         @OA\JsonContent(ref="#/components/schemas/CreateProblem"),
 *     ),
 *     @OA\Response(
 *         response=405,
 *         description="Invalid input",
 *     @OA\JsonContent(
 *     @OA\Property(
 *          property="user",
 *          ref="#/components/schemas/user"),
 *        @OA\Property(
 *          property="return state",
 *          ref="#/components/schemas/ReturnCode"
 *        ))),
 *     security={{"petstore_auth":{"write:pets", "read:pets"}}}
 * )
 */
/**
 * @OA\Schema(schema="CreateProblem",
 *     @OA\Property(
 *     property="problem",
 *     ref="#/components/schemas/CreateProblemInput"),
 *     @OA\Property(
 *     property="reporters",
 *     type="array",
 *     @OA\Items(ref="#/components/schemas/Reporter")))
 */
/**
 * @OA\Post(path="/modify_problem",
 *     description="",
 *     @OA\RequestBody(
 *         description="Pet object that needs to be added to the store",
 *         required=true,
 *         @OA\JsonContent(ref="#/components/schemas/ModifyProblem"),
 *     ),
 *     @OA\Response(
 *         response=405,
 *         description="Invalid input",
 *     @OA\JsonContent(
 *        @OA\Property(
 *          property="return state",
 *          ref="#/components/schemas/ReturnCode"
 *        ),
 *        @OA\Property(
 *          property="user",
 *          ref="#/components/schemas/user"))),
 *     security={{"petstore_auth":{"write:pets", "read:pets"}}}
 * )
 */
/**
 * @OA\Schema(schema="ModifyProblem",
 *     @OA\Property(
 *     property="problem",
 *     ref="#/components/schemas/ModifyProblemInput"),
 *     @OA\Property(
 *     property="reporters",
 *     type="array",
 *     @OA\Items(ref="#/components/schemas/Reporter")))
 */


/**
 * @Schema(schema="CreateProblemInput",
 * @Property(
 *     property="user_id",
 *     type="string",
 *     format="uuid"),
 * @Property(
 *     property="problem_description",
 *     type="string"),
 * @Property(
 *     property="problem_status",
 *     type="integer",
 *     enum={"0", "1", "2", "3","4"}),
 * @Property(
 *     property="service_ids",
 *     type="array",
 *     @OA\Items(type="string",format="uuid")),
 * @Property(
 *     property="reporters_ids",
 *     type="array",
 *     @OA\Items(type="string",format="uuid")),
 * @Property(
 *     property="labels_id",
 *     type="array",
 *     @OA\Items(type="string",format="uuid")))
 */
/**
 * @Schema(schema="ModifyProblemInput",
 * @Property(
 *     property="user_id",
 *     type="string",
 *     format="uuid"),
 * @Property(
 *     property="problem_id",
 *     type="string",
 *     format="uuid"),
 * @Property(
 *     property="problem_description",
 *     type="string"),
 * @Property(
 *     property="problem_status",
 *     type="integer",
 *     enum={"0", "1", "2", "3","4"}),
 * @Property(
 *     property="service_ids",
 *     type="array",
 *     @OA\Items(type="string",format="uuid")),
 * @Property(
 *     property="reporters_ids",
 *     type="array",
 *     @OA\Items(type="string",format="uuid")),
 * @Property(
 *     property="labels_id",
 *     type="array",
 *     @OA\Items(type="string",format="uuid")))
 */
/**
 * @Schema(schema="user",
 *     @Property(
 *     property="user_id",
 *     type="string",
 *     format="uuid"),
 * @Property(
 *     property="f_name",
 *     type="string"),
 * @Property(
 *     property="l_name",
 *     type="string"),
 * @Property(
 *     property="user_email",
 *     type="string",
 *     format="email"),
 * @Property(
 *     property="mail_setting",
 *     type="boolean"),
 *  @Property(
 *     property="service_setting",
 *     type="string",
 *     format="uuid"),
 *  @Property(
 *     property="language_setting",
 *     type="string",
 *     enum={"NL", "FR", "EN"}),
 * @OA\Property(
 *     property="problems",
 *     type="array",
 *     @OA\Items(ref="#/components/schemas/Problem")),
 * @OA\Property(
 *     property="services",
 *     type="array",
 *     @OA\Items(ref="#/components/schemas/Service")),
 * @OA\Property(
 *     property="reporters",
 *     type="array",
 *     @OA\Items(ref="#/components/schemas/Reporter")),
 * @OA\Property(
 *     property="labels",
 *     type="array",
 *      @OA\Items(ref="#/components/schemas/Label")))
 */
/**
 * @Schema(schema="Problem",
 * @Property(
 *     property="problem_id",
 *     description="id of the problem",
 *     type="string",
 *     format="uuid"),
 * @Property(
 *     property="user_id",
 *     description="id of the user",
 *     type="string",
 *     format="uuid"),
 * @Property(
 *     property="problem_description",
 *     description="description of the problem",
 *     type="string"),
 * @Property(
 *     property="problem_status",
 *     description="status of the problem",
 *     type="integer",
 *     enum={"0", "1", "2", "3","4"}),
 * @Property(
 *     property="creation_date",
 *     description="creation date of the problem",
 *     type="string",
 *     format="date-time"),
 * @Property(
 *     property="last_notified",
 *     description="last motified date of the problem",
 *     type="string",
 *     format="date-time"),
 * @Property(
 *     property="service_ids",
 *     description="service_ids of the problem",
 *     type="array",
 *     @OA\Items(type="string",format="uuid")),
 * @Property(
 *     property="reporters_ids",
 *     description="reporter_ids of the problem",
 *     type="array",
 *     @OA\Items(type="string",format="uuid")),
 * @Property(
 *     property="labels_id",
 *     description="label_ids of the problem",
 *     type="array",
 *     @OA\Items(type="string",format="uuid")))
 */
/**
 * @Schema(schema="Service",
 * @Property(
 *     property="service_id",
 *     type="string",
 *     format="uuid"),
 * @Property(
 *     property="user_id",
 *     type="string",
 *     format="uuid"),
 * @Property(
 *     property="service_description",
 *     type="string",
 *     format="uuid"),
 * @Property(
 *     property="service_email",
 *     type="string",
 *     format="email"))
 */
/**
 * @Schema(schema="Label",
 * @Property(
 *     property="label_id",
 *     type="string"),
 * @Property(
 *     property="label_name",
 *     type="string"),
 * @Property(
 *     property="user_id",
 *     type="string"))
 */
/**
 * @Schema(schema="Reporter",
 *     required={"reporter_id"},
 * @Property(
 *     property="reporter_id",
 *     type="string"),
 * @Property(
 *     property="reporter_email",
 *     type="string"),
 * @Property(
 *     property="reporter_f_name",
 *     type="string"),
 * @Property(
 *     property="reporter_l_name",
 *     type="string"),
 * @Property(
 *     property="reporter_phone1",
 *     type="integer"),
 * @Property(
 *     property="reporter_phone2",
 *     type="integer"),
 *  @Property(
 *     property="address",
 *     ref="#/components/schemas/Address"))
 */
/**
 * @Schema(schema="Address",
 * @Property(
 *     property="street",
 *     type="string"),
 * @Property(
 *     property="number",
 *     type="integer"),
 * @Property(
 *     property="city",
 *     type="string"),
 * @Property(
 *     property="zipcode",
 *     type="integer"))
 */
/**
 * @OA\Schema(schema="ReturnCode",
 *     required={"return code", "msg"},
 *   @OA\Property(
 *     property="return code",
 *     type="integer",),
 *   @OA\Property(
 *     property="msg",
 *     type="string")
 * )
 */