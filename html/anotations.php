<?php
/**
 * Created by PhpStorm.
 * User: barteld.logghe
 * Date: 8-7-2019
 * Time: 21:51
 */


require("../vendor/autoload.php");
$openapi = \OpenApi\scan('/path/to/project');
header('Content-Type: application/x-yaml');
echo $openapi->toYaml();