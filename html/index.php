<?php
/**
 * @OA\Info(title="App_name", version="0.1")
 */

namespace Classes;

require __DIR__ . '/../vendor/autoload.php';

require_once __DIR__ . '/../classes/require.php';
require_once __DIR__ . '/../classes/controller.php';
require_once __DIR__ . '/../classes/Mailer.php';

use Slim\Slim;
use Firebase\JWT\JWT;

//use OpenApi\Annotations as OA;
/** @var \Slim\Slim $appCreateProblem */
$app = new Slim();

$app->view( new \JsonApiView() );
$app->add( new \JsonApiMiddleware() );
$app->response->headers->set( 'Content-Type', 'application/json; charset=utf-8' );
//$encrypt= new \classes\Encrypt("exposure-f56qsd6qs4d856q4sdf4qz4fajoijfdihg)aç!yruça","HS256" );

//Testing
$app->get( '/v1/mail_test', function () use ( $app ) {
	$user_id = $app->request->get( 'user_id' );
	try {
		$mailer = new Mailer();
		//todo has to be called in controller after creation problem.
		$mailer->addReporterMails("exposurefromtest@gmail.com");
		$mailer->addReporterMails("receiver@blogghe.be");
		$mailer->addServiceMails("exposureservicetest@gmail.com");
		$mailer->addServiceMails("receiver@blogghe.be");
		$userEmail="logghebarteld@gmail.com";
		$mailer->AlertAfterCreationProblem($userEmail);
	} catch ( \PHPMailer\PHPMailer\Exception $e ) {
		var_dump($e->errorMessage());
	}catch (\Exception $e){
		var_dump($e->getMessage());
	}

	$app->response->body( json_encode( "" ) );

	//var_dump($controller->getMailer());
} );
$app->get( '/v1/generateTokenForUser', function () use ( $app ) {
	$user_id = $app->request->get( 'user_id' );
	$controller = new controller();
	$controller->getEncrypt()->GenerateTokenForUser($user_id);



die();
} );

$app->get('/v1/testAuthorizationForUser',function () use ( $app ){
	$controller = new controller();
	$user_id = $app->request->get( 'user_id' );
	if ($controller->getEncrypt()->checkAuthorization($user_id)) {
		$controller->setUser(new User());
		$controller->getUser()->setUserId($user_id);
		//do something
		$controller->setReturnMessage(new ReturnMessage());
		$controller->getReturnMessage()->setStatus("200","auth was ok");
	}else{
		$controller->setReturnMessage($controller->getEncrypt()->getReturnMessage());
		$app->response->setStatus(401);
	}
	$app->response->body(json_encode($controller->ReturnMsg()));
});

//end testing


//Get-query changes
$app->get( '/get_user_info2', function () use ( $app ) {
	$controller = new controller();
	$user_id = $app->request->get( 'user_id' );
	$controller->setUser( new User() );
	$controller->getUser()->setUserId( $user_id );
	$controller->getUserInfo();
	$app->response->body( json_encode( $controller->ReturnMsg() ) );
} );
$app->get( '/get_labels2', function () use ( $app ) {
	$controller = new controller();
	$user_id = $app->request->get( 'user_id' );

	//$user_id = $app->request->get( 'user_id' );
	$controller->setUser( new User() );
	$controller->getUser()->setUserId( $user_id );
	$controller->getLabelsByUser();

	$app->response->body( json_encode( $controller->ReturnMsg() ) );
} );
$app->get( '/view_label', function () use ( $app ) {
	$controller = new controller();
	$user_id = $app->request->get( 'user_id' );
	$label_id = $app->request->get( 'label_id' );

	$controller->setUser( new User() );
	$controller->getUser()->setUserId( $user_id );
	$controller->getLabelInfo($label_id);
	$app->response->body( json_encode( $controller->ReturnMsg() ) );

});

$app->get( '/get_services2', function () use ( $app ) {
	$controller = new controller();
	$user_id = $app->request->get( 'user_id' );
	$controller->setUser( new User() );
	$controller->getUser()->setUserId( $user_id );
	$controller->getServicesByUserId();
	$app->response->body( json_encode( $controller->ReturnMsg() ) );
} );

$app->get( '/view_service', function () use ( $app ) {
	$controller = new controller();
	$user_id = $app->request->get( 'user_id' );
	$service_id = $app->request->get( 'service_id' );

	$controller->setUser( new User() );
	$controller->getUser()->setUserId( $user_id );
	$controller->getServiceInfo($service_id);
	$app->response->body( json_encode( $controller->ReturnMsg() ) );

});

$app->get( '/get_reporters2', function () use ( $app ) {
	$controller = new controller();
	$user_id = $app->request->get( 'user_id' );
	$controller->setUser( new User() );
	$controller->getUser()->setUserId( $user_id );
	$controller->getReportersByUser();
	$app->response->body( json_encode( $controller->ReturnMsg() ) );
});

$app->get( '/view_reporter', function () use ( $app ) {
	$controller = new controller();
	$user_id = $app->request->get( 'user_id' );
	$reporter_id = $app->request->get( 'reporter_id' );

	$controller->setUser( new User() );
	$controller->getUser()->setUserId( $user_id );
	$controller->getReporterInfo($reporter_id);
	$app->response->body( json_encode( $controller->ReturnMsg() ) );
});


$app->get( '/get_problems2', function () use ( $app ) {
	$controller = new controller();
	$user_id = $app->request->get( 'user_id' );
	$controller->setUser( new User() );
	$controller->getUser()->setUserId( $user_id );
	$controller->getProblemsByUser();
	$app->response->body( json_encode( $controller->ReturnMsg() ) );
} );

$app->get( '/view_problem', function () use ( $app ) {
	$controller = new controller();
	$user_id = $app->request->get( 'user_id' );
	$problem_id = $app->request->get( 'problem_id' );
	$controller->setUser( new User() );
	$controller->getUser()->setUserId( $user_id );
	$controller->getOneProblemByUser( $problem_id );
	$app->response->body( json_encode( $controller->ReturnMsg() ) );

} );
$app->get( '/list_problems', function () use ( $app ) {
	$controller = new controller();
	$user_id = $app->request->get( 'user_id' );
	$controller->setUser( new User() );
	$controller->getUser()->setUserId( $user_id );
	$controller->getProblemListByUser();
	$app->response->body( json_encode( $controller->ReturnMsg() ) );
});
$app->get( '/login2', function () use ( $app ) {
	$controller = new controller();
	$user_email = $app->request->get( 'user_email' );
	if (filter_var($user_email, FILTER_VALIDATE_EMAIL)) {
		$user_pass = $app->request->get( 'user_pass' );
		$controller->setUser( new User() );
		$controller->getUser()->setUserEmail( $user_email );
		$controller->Login( $user_pass );
	}else{
		$controller->FalseEmail();
	}
	$app->response->body( json_encode( $controller->ReturnMsg() ) );
} );



$app->get( '/request_pass_change', function () use ( $app ) {
	$controller = new controller();
	$user_email = $app->request->get( 'user_email' );
	if (filter_var($user_email, FILTER_VALIDATE_EMAIL)) {
		$controller->setUser( new User() );
		$controller->getUser()->setUserEmail( $user_email );
	}else{
		$controller->FalseEmail();
	}
	$app->response->body( json_encode( $controller->ReturnMsg() ) );
} );

$app->get( '/status_check2', function () use ( $app ) {
	$controller = new controller();
	$user_id = $app->request->get( 'user_id' );
	$controller->setUser( new User() );
	$controller->getUser()->setUserId( $user_id );
	$controller->StatusCheck();
	$app->response->body( json_encode( $controller->ReturnMsg() ) );
} );

$app->get( '/get_all_info', function () use ( $app ) {
	$controller = new controller();
	$user_id = $app->request->get( 'user_id' );
	$controller->setUser( new User() );
	$controller->getUser()->setUserId( $user_id );

	//todo solve problem with multiple functions doing a rollback
	$controller->getUserInfo();
	$controller->getLabelsByUser();
	$controller->getReportersByUser();
	$controller->getServicesByUserId();
	$controller->getProblemsByUser();
	$app->response->body( json_encode( $controller->ReturnMsg() ) );
} );

$app->get( '/get_problems_states', function () use ( $app ) {
	$controller = new controller();
	$user_id = $app->request->get( 'user_id' );
	$controller->setUser( new User() );
	$controller->getUser()->setUserId( $user_id );
	$controller->getProblemsStatesByUser();
	$app->response->body( json_encode( $controller->ReturnMsg() ) );
});
$app->get( '/send_mails', function () use ( $app ) {
	$controller = new controller();
	$controller->setUser( new User());
	$controller->getUser()->setUserId( $app->request->get('user_id') );
	$controller->getReturnMessage()->setStatus(200, "mails send");
	$app->response->body( json_encode( $controller->ReturnMsg() ) );

});
//todo get problemsByLabel
//todo get problemsByService
//todo get problemsByReporter
//todo get problemsByStatus
//todo get problemsByStatusAndLabel
//todo get problemsByStatusAndService

//Post-command changes
$app->post( '/create_user', function () use ( $app ) {
	$controller = new controller();
	$body = $app->request->getBody();
	header( "Content-Type: application/json" );
	$decoded = json_decode( $body, true );
	$controller->getUser()->setFName( $decoded[ 'f_name' ] );
	$controller->getUser()->setLName( $decoded[ 'l_name' ] );
	$controller->getUser()->setUserEmail( $decoded[ 'user_email' ] );
	if (filter_var($decoded[ 'user_email' ] , FILTER_VALIDATE_EMAIL)) {
		//user_pass is already encrypted with PASSWORD_BCRYPT
		$controller->CreateUser($decoded[ 'user_pass' ]);
	}else{
		$controller->FalseEmail();
	}

	$app->response->body( json_encode( $controller->ReturnMsg() ) );
} );

$app->post( '/modify_user', function () use ( $app ) {
	$controller = new controller();
	$body = $app->request->getBody();
	header( "Content-Type: application/json" );
	$decoded = json_decode( $body, true );
	$controller->getUser()->setUserId( $decoded[ 'user_id' ] );
	$controller->getUser()->setFName( $decoded[ 'f_name' ] );
	$controller->getUser()->setLName( $decoded[ 'l_name' ] );
	$controller->getUser()->setUserEmail( $decoded[ 'user_email' ] );
	$controller->getUser()->setMailSetting($decoded['mail_setting']);
	$controller->getUser()->setServiceSetting($decoded['service_setting']);
	$controller->getUser()->setLanguageSetting($decoded['language_setting']);
	if (filter_var($decoded[ 'user_email' ] , FILTER_VALIDATE_EMAIL)) {
		$controller->ModifyUser();
	}else{
		$controller->FalseEmail();
	}
	$app->response->body( json_encode( $controller->ReturnMsg() ) );

} );

$app->post( '/create_label', function () use ( $app ) {
	$controller = new controller();
	$body = $app->request->getBody();
	header( "Content-Type: application/json" );
	$decoded = json_decode( $body, true );

	$controller->getUser()->setUserId( $decoded[ 'user_id' ] );
	$controller->CreateLabel( $decoded[ 'label_name' ] );
	$app->response->body( json_encode( $controller->ReturnMsg() ) );
} );

$app->post( '/modify_label', function () use ( $app ) {
	$controller = new controller();
	$body = $app->request->getBody();
	header( "Content-Type: application/json" );
	$decoded = json_decode( $body, true );

	$controller->getUser()->setUserId( $decoded[ 'user_id' ] );
	$controller->ModifyLabel( $decoded[ 'label_id' ], $decoded[ 'label_name' ] );
	$app->response->body( json_encode( $controller->ReturnMsg() ) );
} );

$app->post( '/create_service', function () use ( $app ) {
	$controller = new controller();
	$body = $app->request->getBody();
	header( "Content-Type: application/json" );
	$decoded = json_decode( $body, true );
	$controller->getUser()->setUserId( $decoded[ 'user_id' ] );
	if (filter_var($decoded[ 'service_email' ] , FILTER_VALIDATE_EMAIL)) {
		$controller->CreateService( $decoded[ 'service_email' ], $decoded[ 'service_description' ] );
	}else{
		$controller->FalseEmail();
	}
	$app->response->body( json_encode( $controller->ReturnMsg() ) );
} );

$app->post( '/modify_service', function () use ( $app ) {
	$controller = new controller();
	$body = $app->request->getBody();
	header( "Content-Type: application/json" );
	$decoded = json_decode( $body, true );

	$controller->getUser()->setUserId( $decoded[ 'user_id' ] );
	if (filter_var($decoded[ 'service_email' ] , FILTER_VALIDATE_EMAIL)) {
		$controller->ModifyService( $decoded[ 'service_id' ], $decoded[ 'service_email' ], $decoded[ 'service_description' ] );
	}else{
		$controller->FalseEmail();
	}
	$app->response->body( json_encode( $controller->ReturnMsg() ) );
} );

$app->post( '/create_reporter', function () use ( $app ) {
	$controller = new controller();
	$body = $app->request->getBody();
	header( "Content-Type: application/json" );
	$decoded = json_decode( $body, true );

	$controller->getUser()->setUserId( $decoded[ 'user_id' ] );
	$controller->CreateReporter( $decoded[ 'reporter_f_name' ], $decoded[ 'reporter_l_name' ], $decoded[ 'reporter_email' ],$decoded[ 'reporter_phone1' ],$decoded[ 'reporter_phone2' ] );
	$app->response->body( json_encode( $controller->ReturnMsg() ) );
} );

$app->post( '/modify_reporter', function () use ( $app ) {
	$controller = new controller();
	$body = $app->request->getBody();
	header( "Content-Type: application/json" );
	$decoded = json_decode( $body, true );

	$controller->getUser()->setUserId( $decoded[ 'user_id' ] );
	$controller->ModifyReporter( $decoded[ 'reporter_id' ], $decoded[ 'reporter_f_name' ], $decoded[ 'reporter_l_name' ], $decoded[ 'reporter_email' ],$decoded[ 'reporter_phone1' ],$decoded[ 'reporter_phone2' ] );
	$app->response->body( json_encode( $controller->ReturnMsg() ) );
} );

$app->post( '/change_pass', function () use ( $app ) {
	$controller = new controller();
	$body = $app->request->getBody();
	header( "Content-Type: application/json" );
	$decoded = json_decode( $body, true );
	$controller->getUser()->setUserId( $decoded[ 'user_id' ] );
	$user_pass = $decoded[ 'user_pass' ];
	$controller->ChangePass( $user_pass );
	$app->response->body( json_encode( $controller->ReturnMsg() ) );
} );

$app->post( '/create_problem', function () use ( $app ) {

	$controller = new controller();
	$body = $app->request->getBody();
	header( "Content-Type: application/json" );
	$decoded = json_decode( $body, true );
	$controller->getUser()->setUserId( $decoded[ 'problem' ][ 'user_id' ] );
	$service_ids = $decoded[ 'problem' ][ 'service_ids' ];
	$label_ids = $decoded[ 'problem' ][ 'label_ids' ];
	$reporter_ids = $decoded[ 'problem' ][ 'reporter_ids' ];
	$problem_description = $decoded[ 'problem' ][ 'problem_description' ];
	$problem_status = $decoded['problem']['problem_status'];
	$reporters = $decoded[ 'reporters' ];
	$controller->CreateProblem( $service_ids, $reporter_ids, $label_ids, $problem_description, $problem_status,$reporters );
	$app->response->body( json_encode( $controller->ReturnMsg() ) );

} );

$app->post( '/modify_problem', function () use ( $app ) {

	$controller = new controller();
	$body = $app->request->getBody();
	header( "Content-Type: application/json" );
	$decoded = json_decode( $body, true );
	$controller->getUser()->setUserId( $decoded[ 'problem' ][ 'user_id' ] );
	$service_ids = $decoded[ 'problem' ][ 'service_ids' ];
	$label_ids = $decoded[ 'problem' ][ 'label_ids' ];
	$reporter_ids = $decoded[ 'problem' ][ 'reporter_ids' ];
	$problem_id = $decoded[ 'problem' ][ 'problem_id' ];
	$problem_description = $decoded[ 'problem' ][ 'problem_description' ];
	$problem_status = $decoded['problem']['problem_status'];
	$reporters = $decoded[ 'reporters' ];
	$controller->ModifyProblem( $problem_id, $service_ids, $reporter_ids, $label_ids, $problem_description,$problem_status,$reporters );
	$app->response->body( json_encode( $controller->ReturnMsg() ) );
} );

$app->post( '/update_status', function () use ( $app ) {
	$controller = new controller();
	//todo impelement
	$body = $app->request->getBody();

	header( "Content-Type: application/json" );

} );

//todo remove ProblemServiceRelation
//todo remove ProblemReporterRelation
//todo remove ServiceLabelProblemRelation


/**
 * @OA\Get(path="/api/posts",
 *   @OA\Response(
 *      response=200,
 *      description="List postssss",
 *      @OA\JsonContent(
 *        @OA\Property(
 *          property="posts",
 *          type="array",
 *          @OA\Items(ref="#/components/schemas/brol")
 *        ),
 *        @OA\Property(
 *          property="ReturnCode",
 *          ref="#/components/schemas/ReturnCode"
 *        )
 *      )
 *   )
 * )
 */

/**
 * @OA\Schema(
 *   schema="brol",
 *   @OA\Property(
 *     property="id",
 *     type="integer"
 *   )
 * )
 */

/**
 * @OA\Schema(
 *   schema="ReturnCode",
 *   @OA\Property(
 *     property="return code",
 *     type="integer",
 *     minimum=1
 *   ),
 *   @OA\Property(
 *     property="msg",
 *     type="string",
 *     minimum=1
 *   )
 * )
 */

/**
 * @OA\Schema(
 *   schema="MailsSend",
 *   @OA\Property(
 *     property="confirmed_mails",
 *     type="integer",
 *     minimum=1
 *   ),
 *   @OA\Property(
 *     property="ongoing_mails",
 *     type="integer",
 *     minimum=1
 *   )
 * )
 */

$app->run();